import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';

import { Usuario } from '../../models/usuario.model';
import { Abrigo } from '../../models';

@Injectable()
export class UsuarioService {
  public currentAbrigo: Abrigo;
  public currentUser: Usuario;
  private baseUrl = '/usuario';
  constructor(private authHttp: AuthHttp) {
    this.getAll().then((res) => {
      // console.log(res);
    }).catch((err) => {

    });
  }

  public create(usuario: Usuario) {
    let data: any = new Object();
    Object.assign(data,usuario);
    return this.authHttp.post(this.baseUrl, data);
  }

  public edit(usuario: Usuario) {
    let data: any = new Object();
    Object.assign(data,usuario);
    return this.authHttp.put(`${this.baseUrl}/${usuario._id}`, data);
  }

  public delete(usuario: any) {
    return this.authHttp.delete(`${this.baseUrl}/${usuario._id}`);
  }

  public getByID(id: string) {
    return this.authHttp.get(`${this.baseUrl}/${id}`);
  }


  public getAll() {
    return this.authHttp.get(this.baseUrl);
  }

  public read(id) {
    return this.authHttp.get(this.baseUrl);
  }

  public setUser(user) {
    let usuario = new Usuario;
    Object.assign(usuario, user);
    this.currentUser = usuario;
  }

  public setAbrigo(abrigo) {
    let newAbrigo = new Abrigo();
    Object.assign(newAbrigo, abrigo);
    this.currentAbrigo = newAbrigo;
  }

  public getAbrigo() {
    return this.currentAbrigo;
  }

  public getUser() {
    return this.currentUser;
  }

}
