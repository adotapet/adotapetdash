import { Component, OnInit } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';
import { Router } from '@angular/router';
import { NavService } from '../../../services/navigation.service';

@Component({
  selector: 'app-solicitacao-list',
  templateUrl: './solicitacao-list.component.html',
  styleUrls: ['./solicitacao-list.component.scss']
})
export class SolicitacaoListComponent implements OnInit {
  solicitacao = new Solicitacao();
  constructor(public router:Router, public nav: NavService) { }

  ngOnInit() {
  }

  returnSituacao(i) {
    let status = [{id: 1, label: 'Teste'}];
    let find =  status.find((elem) => { return elem.id === i });
    return find ? find.label : 'indefinido';
  }

  add() {
    this.router.navigate(['dash/solicitacao/add']);
  }

  edit(model) {
    this.nav.clearParameters();
    this.nav.setParameters('model', model);
    this.router.navigate(['/dash/solicitacao/edit']);
  }

  read() {
    this.router.navigate(['dash/solicitacao/read']);
  }

  limparFiltros() {

  }

}
