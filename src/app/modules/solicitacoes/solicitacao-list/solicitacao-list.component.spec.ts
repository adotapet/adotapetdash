import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdocaoListComponent } from './adocao-list.component';

describe('AdocaoListComponent', () => {
  let component: AdocaoListComponent;
  let fixture: ComponentFixture<AdocaoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdocaoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdocaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
