import { Component, OnInit } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';

@Component({
  selector: 'app-solicitacao-read',
  templateUrl: './solicitacao-read.component.html',
  styleUrls: ['./solicitacao-read.component.scss']
})
export class SolicitacaoReadComponent implements OnInit {
  model: Solicitacao = new Solicitacao();
  visitante = false;
  constructor() { }

  ngOnInit() {
  }

}
