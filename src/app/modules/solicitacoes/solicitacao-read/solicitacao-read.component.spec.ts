import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdocaoReadComponent } from './adocao-read.component';

describe('AdocaoReadComponent', () => {
  let component: AdocaoReadComponent;
  let fixture: ComponentFixture<AdocaoReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdocaoReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdocaoReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
