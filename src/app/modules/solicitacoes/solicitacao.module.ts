import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { TabsModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { SolicitacaoListComponent } from './solicitacao-list/solicitacao-list.component';
import { SolicitacaoAddComponent } from './solicitacao-add/solicitacao-add.component';
import { SolicitacaoEditComponent } from './solicitacao-edit/solicitacao-edit.component';
import { SolicitacaoReadComponent } from './solicitacao-read/solicitacao-read.component';
import { SolicitacaoService } from 'src/app/modules/adocoes/services/solicitacao.service';

const routes: Routes = [
  { path: '', redirectTo: 'list'},
  { path: 'list', component: SolicitacaoListComponent },
  { path: 'add', component: SolicitacaoAddComponent },
  { path: 'edit', component: SolicitacaoEditComponent },
  { path: 'read', component: SolicitacaoReadComponent },
  // { path: 'edit', loadChildren: './abrigo-edit/abrigo-edit.module#AbrigoEditModule' },
  // { path: 'consultorio', loadChildren: './modules/consultorio/consultorio.module#ConsultorioModule' },
  // { path: 'medicos', loadChildren: './modules/medico/medico.module#MedicoModule'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot()
  ],
  declarations: [SolicitacaoAddComponent, SolicitacaoListComponent, SolicitacaoEditComponent, SolicitacaoReadComponent],
  providers: [
    FormCustomValidator,
    ToastrService,
  ]
})
export class SolicitacaoModule { }
