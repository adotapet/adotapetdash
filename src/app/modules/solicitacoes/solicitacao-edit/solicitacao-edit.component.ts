import { Component, OnInit } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-solicitacao-edit',
  templateUrl: './solicitacao-edit.component.html',
  styleUrls: ['./solicitacao-edit.component.scss']
})
export class SolicitacaoEditComponent implements OnInit {
  model: Solicitacao = new Solicitacao();
  form: FormGroup;

  // statusOpcoes = this.model.getStatus();
  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    // this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao salvar adoção');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/solicitacao']);
  }

}
