import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdocaoEditComponent } from './adocao-edit.component';

describe('AdocaoEditComponent', () => {
  let component: AdocaoEditComponent;
  let fixture: ComponentFixture<AdocaoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdocaoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdocaoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
