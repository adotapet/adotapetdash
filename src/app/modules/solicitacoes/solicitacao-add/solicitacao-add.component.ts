import { Component, OnInit } from '@angular/core';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';
import { Solicitacao } from '../../../models/solicitacao.model';

@Component({
  selector: 'app-solicitacao-add',
  templateUrl: './solicitacao-add.component.html',
  styleUrls: ['./solicitacao-add.component.scss']
})
export class SolicitacaoAddComponent implements OnInit {
  model: Solicitacao = new Solicitacao();

  form: FormGroup;

  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    // this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao registrar adoção');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/solicitacao']);
  }

}
