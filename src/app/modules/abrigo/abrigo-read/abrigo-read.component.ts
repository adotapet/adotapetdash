// const EstadosJson = require('../../../json/estados-cidade.json');
// const PaisesJson = require('../../../json/paises.json');
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Abrigo } from '../../../models/abrigo.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { AbrigoService } from '../abrigo.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { Local } from 'protractor/built/driverProviders';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-abrigo-read',
  templateUrl: './abrigo-read.component.html',
  styleUrls: ['./abrigo-read.component.scss']
})
export class AbrigoReadComponent implements OnInit {

  model: Abrigo = new Abrigo();
  estados: any[] = [];
  cidades: any[] = [];
  paises: any[] = [];
  jsonEstadoData: any;
  blobPerfil: any;

  constructor(
    private router: Router,
    private authHttp: AuthHttp,
    private http: HttpClient,
    private abrigoService: AbrigoService,
    private usuarioService: UsuarioService,
    private sanitizer: DomSanitizer,
    private toast: ToastrService,
    private localStorage: LocalStorageService
  ) {


    this.getEstados().subscribe((data) => {
      this.jsonEstadoData = data.estados;
      this.estados = data.estados.map((elem) => elem);
    });

    this.getPaises().subscribe((data) => {
      this.paises = data;
    });
   }

  async ngOnInit() {
    if (this.usuarioService.getUser()) {
      let abrigo = await this.abrigoService.getAbrigoByUserID(this.usuarioService.getUser()._id);
			this.abrigoService.abrigo = abrigo;
      this.model = abrigo;
      this.localStorage.setAbrigoLogged(abrigo);
    }
    this.carregarFoto();
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  carregarFoto() {
    if (this.model.fotoPerfil) {
      const loading = this.toast.info('Carregando foto....');
      this.authHttp.getFile(this.model.fotoPerfil ? this.model.fotoPerfil._id || this.model.fotoPerfil : null).then(result => {
        this.blobPerfil = result;
        this.toast.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toast.clear(loading.toastId);
      });
    }
  }

  updateCidades(estado) {
    this.cidades = estado ? estado.cidades : [];
  }

  edit() {
    this.router.navigate(['/dash/abrigo/edit']);
  }

  private getEstados(): Observable<any> {
    return this.http.get('assets/estados-cidade.json');
  }

  private getPaises(): Observable<any> {
    return this.http.get('assets/paises.json');
  }

}
