// const EstadosJson = require('../../../json/estados-cidade.json');
// const PaisesJson = require('../../../json/paises.json');
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Abrigo } from '../../../models/abrigo.model';
import { AbrigoService } from '../abrigo.service';
import { UsuarioService } from '../../usuario/usuario.service';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { MaskService } from '../../../core/mask.service';
import { Endereco } from '../../../models';
import { EnderecoService } from '../../../services/models-services/endereco.service';
import { MessagesCore } from '../../../shared/class/messagesCore.class';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalStorageService } from 'src/app/services/local-storage.service';

// let EstadosJson = [];
// let PaisesJson = [];

@Component({
  selector: 'app-abrigo-edit',
  templateUrl: './abrigo-edit.component.html',
  styleUrls: ['./abrigo-edit.component.scss']
})
export class AbrigoEditComponent implements OnInit {
  abrigo: Abrigo = new Abrigo();
  endereco: Endereco = new Endereco();
  estados: any[] = [];
  cidades: any[] = [];
  paises: any[] = [];
  
  estadoSelecionado: any;
  paisSelecionado: any;
  jsonEstadoData: any;
  
  form: FormGroup;
  formNewEndereco: FormGroup;
  mask: any = {};

  blobPerfil: any;

  constructor(
    private router: Router,
    private http: HttpClient,
    private authHttp: AuthHttp,
    private abrigoService: AbrigoService,
    private usuarioService: UsuarioService,
    private enderecoService: EnderecoService,
    private formValidate: FormCustomValidator,
    private toat: ToastrService,
    private maskService: MaskService,
    private sanitizer: DomSanitizer,
    private localStorage: LocalStorageService
  ) {

    this.abrigoService.abrigo ? this.abrigo.fromApi(this.abrigoService.abrigo) : this.router.navigate(['/dash']);
    this.endereco.fromApi(this.abrigo.endereco);
    console.log('endereco', this.endereco);
    this.getEstados().subscribe((data) => {
      this.jsonEstadoData = data.estados;
      this.estados = data.estados;
    });

    this.getPaises().subscribe((data) => {
      this.paises = data.filter((elem) => elem.nome_pais === 'Brasil');
    });
   }

  ngOnInit() {
    const services = [];
    services['abrigo'] = this.abrigoService;
    this.form = this.formValidate.createFormGroup(this.abrigo.toFormGroup(services, true));
    this.formNewEndereco = this.formValidate.createFormGroup(this.endereco.toFormGroup());
    this.initMask();
    console.log(this.abrigo);
    this.carregarFoto();
  }

  carregarFoto() {
    if (this.abrigo.fotoPerfil) {
      const loading = this.toat.info('Carregando foto....');
      this.authHttp.getFile(this.abrigo.fotoPerfil ? this.abrigo.fotoPerfil._id || this.abrigo.fotoPerfil : null).then(result => {
        this.blobPerfil = result;
        this.toat.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toat.clear(loading.toastId);
      });
    }
  }

  initMask() {
    this.mask.email = this.maskService.emailMask;
    this.mask.cnpj = this.maskService.cnpjMask;
    this.mask.telefone = this.maskService.telefoneMask;
    this.mask.cep = this.maskService.cepMask;
  }

  updateCidades(estado) {
    this.cidades = estado.cidades;
  }

  async save() {
    let backups = {};
    if (this.form.valid && this.formNewEndereco.valid) {
      if (this.abrigo.funcionarios) {
        this.abrigo.fromFormGroup(this.form);
        this.endereco.fromFormGroup(this.formNewEndereco);
        const foto = await this.saveFoto();
        this.abrigo.fotoPerfil = foto ? foto._id : null;
        this.enderecoService.edit(this.endereco)
        .then((res: any) => {
          if (this.formValidate.checkResultError(res)) {
            throw new Error(res.message);
          }
          this.abrigoService.edit(this.abrigo)})
        .then((res: any) => {
          if (this.formValidate.checkResultError(res)) {
            throw new Error(res.message);
          }
          this.toat.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesEdit);
          this.goBack();
        }).catch((err) => {
          this.toat.error(MessagesCore.tituloError, MessagesCore.msgErrorEdit);
          console.log(err);
        });
      } else {
        this.abrigo.funcionarios = [];
        this.abrigo.funcionarios.push(this.usuarioService.getUser()._id);
        this.abrigoService.create(this.abrigo).then((res) => {
        this.toat.success('Edição realizada com sucesso!');          
          this.goBack();
        }).catch((err) => {
          this.toat.error('Erro ao realizar a operação!');
          console.log(err);
        });

      }
    } else {
      this.toat.error('Formulário inválido!');
    }
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  saveFoto(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.blobPerfil) {
        resolve(await this.authHttp.uploadFile(this.blobPerfil));
      } else {
        resolve(null);
      }
    });
  }

  uploadFile(event) {
    let file = event.target.files[0];

    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      this.blobPerfil = new Blob([reader.result], {type: 'application/octet-stream'});
      console.log(this.blobPerfil);
      // this.http.uploadFile(blob).then((data: any) => {
      //   this.source = this.sanitizer.bypassSecurityTrustUrl(data);
      // });
    }
  }

  goBack() {
    this.router.navigate(['/dash/abrigo/read']);
  }

  private getEstados(): Observable<any> {
    return this.http.get('assets/estados-cidade.json');
  }

  private getPaises(): Observable<any> {
    return this.http.get('assets/paises.json');
  }

  validate() {
    return (this.abrigo.nome && this.abrigo.email && this.abrigo.telefone &&
      this.abrigo.descricao && this.abrigo.endereco && this.abrigo.responsavel);
  }

}
