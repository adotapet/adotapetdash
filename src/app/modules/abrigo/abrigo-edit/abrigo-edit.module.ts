import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AbrigoEditComponent } from './abrigo-edit.component';
import { Routes, RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { TabsModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AbrigoModule } from '../abrigo.module';
import { UsuarioModule } from '../../usuario/usuario.module';
import { FormUserFeedbackModule } from '../../../shared/modules/form-user-feedback/form-user-feedback.module';
import { MaskService } from '../../../core/mask.service';
import { TextMaskModule } from 'angular2-text-mask';

const routes: Routes = [
  { path: '', component: AbrigoEditComponent},
];

@NgModule({
  imports: [
    TextMaskModule,
    RouterModule.forChild(routes),
    TabsModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    FormUserFeedbackModule,
    CommonModule,
    NgSelectModule,
    HttpClientModule,
    UsuarioModule,
    AbrigoModule
  ],
  declarations: [AbrigoEditComponent],
  exports: [RouterModule],
  providers: [MaskService]
})
export class AbrigoEditModule { }
