import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';
import { Abrigo } from '../../models/abrigo.model';
import { EnderecoService } from '../../services/models-services/endereco.service';
import { Endereco } from '../../models';


@Injectable()
export class AbrigoService {
  public abrigo: any;
  public filter = { };

  constructor(private authHttp: AuthHttp, private enderecoService: EnderecoService) {
  }

  public create(abrigo: Abrigo) {
    return this.authHttp.post('/abrigo', abrigo);
  }

  public edit(abrigo: Abrigo) {
    return this.authHttp.put(`/abrigo/${abrigo._id}`, abrigo);
  }

  public delete(abrigo: Abrigo) {
    return this.authHttp.delete(`/abrigo/${abrigo._id}`);
  }

  public read(id) {
    return this.authHttp.get('/abrigo/');
  }

  public getAll() {
    return this.authHttp.get('/abrigo');
  }

  public getOneByID(id: string) {
    return this.authHttp.get('/abrigo/' + id);
  }

  public search(filtro: any) {
    return new Promise((resolve, reject) => {
      this.getAll().then(async (result: any) => {
        // await this.popularEnderecos(result);
        console.log(result, '2');
        let abrigosMatch = result.filter((elem) => {
          return  filtro.nome ? elem.nome.toLowerCase().indexOf(filtro.nome.toLowerCase()) != -1 : true &&
                  filtro['endereco.cidade'] ? elem.endereco.cidade === filtro['endereco.cidade'] : true &&
                  filtro['endereco.uf'] ? elem.endereco.uf === filtro['endereco.uf'] : true
         });
        resolve(abrigosMatch);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  private async popularEnderecos(abrigos: Abrigo[]) {
    console.log('1');
    let enderecos: any = await this.enderecoService.getAll();
    abrigos.forEach(el => {
      el.endereco = enderecos.find(end => end._id === el.endereco);
    });
  }

  public getAbrigoByUserID(userId: string): Promise<Abrigo>{
    let abrigoFinded = new Abrigo();
    return new Promise((resolve, reject) => {
      this.getAll().then((result:any) => {
        abrigoFinded = result.find(abrigo => {
          return abrigo.funcionarios ? abrigo.funcionarios.find(user => user.usuario === userId) : null;
        });
        // popula endereço
        if (abrigoFinded && abrigoFinded.endereco && abrigoFinded.endereco._id) {
          return this.enderecoService.getOneById(abrigoFinded.endereco._id);
        } else {
          return null;
        }
      }).then((resultEndereco: any) => {
        if (abrigoFinded) {
          abrigoFinded.endereco = Object.assign(new Endereco(), resultEndereco);
        }
        resolve(abrigoFinded ? abrigoFinded : null);
      }).catch(err => reject(err));
    });
  }

  public setSearch(parameters) {
    this.filter = parameters;
  }

  public getSearch() {
    return this.filter;
  }

}
