import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoacaoAddComponent } from './doacao-add/doacao-add.component';
import { Routes, RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { TabsModule } from 'ngx-bootstrap';
import { DoacaoListComponent } from './doacao-list/doacao-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { DoacaoEditComponent } from './doacao-edit/doacao-edit.component';
import { DoacaoReadComponent } from './doacao-read/doacao-read.component';
import {DoacaoService} from "./doacao.service";


const routes: Routes = [
  { path: '', redirectTo: 'list'},
  { path: 'list', component: DoacaoListComponent },
  { path: 'add', component: DoacaoAddComponent },
  { path: 'edit', component: DoacaoEditComponent },
  { path: 'read', component: DoacaoReadComponent },
  // { path: 'edit', loadChildren: './abrigo-edit/abrigo-edit.module#AbrigoEditModule' },
  // { path: 'consultorio', loadChildren: './modules/consultorio/consultorio.module#ConsultorioModule' },
  // { path: 'medicos', loadChildren: './modules/medico/medico.module#MedicoModule'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot()
  ],
  declarations: [DoacaoAddComponent, DoacaoListComponent, DoacaoEditComponent, DoacaoReadComponent],
  providers: [
    FormCustomValidator,
    ToastrService,
    DoacaoService,
  ]
})
export class DoacaoModule { }
