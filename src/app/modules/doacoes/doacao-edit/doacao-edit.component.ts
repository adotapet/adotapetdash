import { Component, OnInit } from '@angular/core';
import { Doacao } from '../../../models/doacao.model';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-doacao-edit',
  templateUrl: './doacao-edit.component.html',
  styleUrls: ['./doacao-edit.component.scss']
})
export class DoacaoEditComponent implements OnInit {
  model: Doacao = new Doacao();
  form: FormGroup;
  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao salvar doação');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/adocao']);
  }

}
