import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoacaoEditComponent } from './doacao-edit.component';

describe('DoacaoEditComponent', () => {
  let component: DoacaoEditComponent;
  let fixture: ComponentFixture<DoacaoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoacaoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoacaoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
