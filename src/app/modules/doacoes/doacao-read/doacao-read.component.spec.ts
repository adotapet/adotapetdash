import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoacaoReadComponent } from './doacao-read.component';

describe('DoacaoReadComponent', () => {
  let component: DoacaoReadComponent;
  let fixture: ComponentFixture<DoacaoReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoacaoReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoacaoReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
