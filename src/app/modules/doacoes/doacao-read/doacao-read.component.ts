import { Component, OnInit } from '@angular/core';
import { Doacao } from '../../../models/doacao.model';
import {DoacaoService} from "../doacao.service";
import {NavService} from "../../../services/navigation.service";
import {Router} from "@angular/router";
import {Usuario} from "../../../models/usuario.model";
import {UsuarioService} from "../../usuario/usuario.service";
import {AlertsService} from "../../../services/alerts.service";
import {ToastrService} from "ngx-toastr";
import swal from "sweetalert2";

@Component({
  selector: 'app-doacao-read',
  templateUrl: './doacao-read.component.html',
  styleUrls: ['./doacao-read.component.scss']
})
export class DoacaoReadComponent implements OnInit {
  model: Doacao = new Doacao();
  usuario: Usuario = new Usuario();

  constructor(public router: Router,
              public nav: NavService,
              private doacaoService: DoacaoService,
              private alertsService: AlertsService,
              private toast: ToastrService,
              private usuarioService: UsuarioService,) {
  }

  ngOnInit() {
    if (!this.nav.getParameters('modelId')) {
      this.list();
      return;
    }
    this.refreshModel(this.nav.getParameters('modelId'));
  }

  confirmApproveDonation() {
    let subtitle = "Tem certeza que deseja aceitar a doação de " + this.model.quantidade
      + " unidade(s) de " + this.model.item.nome + "?";
    this.alertsService.confirmAlert(this.approveDonation.bind(this), "Você tem certeza?", subtitle);
  }

  approveDonation() {
    this.doacaoService.approve(this.model).then((result: any) => {
      swal({
        title: "Concluído",
        text: "A doação foi aprovada com sucesso. Entre em contato com o doador para combinarem o local e horário para que ele possa estar finalizando a doação.",
        type: 'success',
        showCancelButton: false,
        confirmButtonText: 'OK'
      }).then((result) => {
      });
      this.refreshModel(this.model._id)
    }).catch(err => {
      console.log(err);
    })
  }

  refreshModel(id: String) {
    const loading = this.toast.info('Carregando');
    this.doacaoService.read(id).then((result: any) => {
      this.model = new Doacao();
      this.model.fromApi(result);

      this.usuarioService.getByID(this.model.doador.usuario).then((result) => {
        console.log(result);
        this.usuario = new Usuario();
        this.usuario.fromApi(result);
        this.toast.clear(loading.toastId);
      }).catch((err) => {
        console.log(err);
        this.toast.clear(loading.toastId);
      })
    }).catch(err => {
      console.log(err);
      this.toast.clear(loading.toastId);
    })
  }

  list() {
    this.router.navigate(['dash/doacao/list']);
  }

}
