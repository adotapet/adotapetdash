import { Component, OnInit } from '@angular/core';
import { Doacao } from '../../../models/doacao.model';
import { Router } from '@angular/router';
import { NavService } from '../../../services/navigation.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from '../../../services/local-storage.service';
import { AlertsService } from '../../../services/alerts.service';
import {DoacaoService} from "../doacao.service";
import swal from "sweetalert2";

@Component({
  selector: 'app-doacao-list',
  templateUrl: './doacao-list.component.html',
  styleUrls: ['./doacao-list.component.scss']
})
export class DoacaoListComponent implements OnInit {
  doacoes: Doacao[] = [];
  doacoesFilter: Doacao[] = [];
  nomeInput: string = '';
  categoriaInput: string = '';
  statusInput: string = "Aguardando";
  filtro : any = {};

  constructor(public router: Router,
              public nav: NavService,
              private toast: ToastrService,
              private doacaoService: DoacaoService,
              private alertsService: AlertsService,
              private localStorage: LocalStorageService) {
   }

  ngOnInit() {
    this.all();
  }

  async all() {
    const loading = this.toast.info('Carregando');
    this.doacaoService.getByAbrigoID(this.localStorage.getAbrigoLogged()._id).then((result: any) => {
      this.doacoes = [];
      this.doacoesFilter = [];
      if (result !== null && result !== undefined && result instanceof Array) {
        for (let i = 0; i < result.length; i++) {
          let donation = new Doacao();
          donation.fromApi(result[i]);
          this.doacoes.push(donation);
        }
      }
      this.applyFilter()
      this.toast.clear(loading.toastId);
    }).catch(err => {
      this.toast.clear(loading.toastId);
      console.log(err);
    })
  }

  read(donation: Doacao) {
    this.nav.clearParameters();
    this.nav.setParameters('modelId', donation._id);
    this.router.navigate(['dash/doacao/read']);
  }

  applyFilter() {
    this.nomeInput ? this.filtro.nome = this.nomeInput : delete this.filtro.nome;
    this.statusInput ? this.filtro.status = this.statusInput : delete this.filtro.status;
    this.categoriaInput ? this.filtro.categoria = this.categoriaInput : delete this.filtro.categoria;
    this.search();
  }

  search() {
    if (this.filtro.status == "Todos") {
      delete this.filtro.status;
    }

    console.log(this.filtro);


    this.doacoesFilter = this.doacoes.filter((elem) => {
      return this.filtro.nome ? elem.item.nome.toLowerCase().indexOf(this.filtro.nome.toLowerCase()) != -1 : true &&
      this.filtro.status ? elem.status.toLowerCase().indexOf(this.filtro.status.toLowerCase()) != -1 : true &&
      this.filtro.categoria ? elem.item.categoria.toLowerCase().indexOf(this.filtro.categoria.toLowerCase()) != -1 : true
    });
  }

  confirmApproveDonation(donation) {
    let subtitle = "Tem certeza que deseja aceitar a doação de " + donation.quantidade
      + " unidade(s) de " + donation.item.nome + "?";
    this.alertsService.confirmAlert(this.approveDonation.bind(this, donation), "Você tem certeza?", subtitle);
  }

  approveDonation(donation: Doacao) {
    this.doacaoService.approve(donation).then((result: any) => {
      swal({
        title: "Concluído",
        text: "A doação foi aprovada com sucesso. Entre em contato com o doador para combinarem o local e horário para que ele possa estar finalizando a doação.",
        type: 'success',
        showCancelButton: false,
        confirmButtonText: 'OK'
      }).then((result) => {
      });
      this.all()
    }).catch(err => {
      console.log(err);
    })
  }

  limparFiltros() {
    this.nomeInput = '';
    this.statusInput = null;
    this.categoriaInput = '';
    this.applyFilter();
  }

}
