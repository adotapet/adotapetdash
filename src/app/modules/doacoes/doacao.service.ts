import {Injectable} from '@angular/core';
import {AuthHttp} from '../../services/auth.http.service';
import {Funcionario} from '../../models/funcionario.model';
import {error} from 'util';
import {Doacao} from "../../models/doacao.model";


@Injectable()
export class DoacaoService {
  baseUrl = '/doacao';

  constructor(private authHttp: AuthHttp) {
  }

  public create(doacao: Doacao) {
    return this.authHttp.post(this.baseUrl, doacao);
  }

  public edit(doacao: Doacao) {
    return this.authHttp.put(`${this.baseUrl}/${doacao._id}`, doacao);
  }

  public read(id) {
    return this.authHttp.get(`${this.baseUrl}/${id}`);
  }

  public delete(doacao: Doacao) {
    return this.authHttp.delete(`/doacao/${doacao._id}`);
  }

  public cancel(doacao: Doacao) {
    doacao.status = "Cancelada";
    return this.authHttp.put(`/doacao/${doacao._id}`, doacao);
  }

  public approve(doacao: Doacao) {
    doacao.status = "Finalizada";
    return this.authHttp.put(`/doacao/${doacao._id}`, doacao);
  }

  public getProducts() {
    return this.authHttp.get(this.baseUrl + "/static/item");
  }

  public getByUserID(usuarioID: string) {
    return this.authHttp.get(this.baseUrl + "?doador=" + usuarioID);
  }

  public getByAbrigoID(abrigo: string) {
    return this.authHttp.get(this.baseUrl + "?abrigo=" + abrigo);
  }

}
