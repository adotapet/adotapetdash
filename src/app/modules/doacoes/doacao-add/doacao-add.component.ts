import { Component, OnInit } from '@angular/core';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { Doacao } from '../../../models/doacao.model';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';

@Component({
  selector: 'app-doacao-add',
  templateUrl: './doacao-add.component.html',
  styleUrls: ['./doacao-add.component.scss']
})
export class DoacaoAddComponent implements OnInit {
  model: Doacao = new Doacao();

  form: FormGroup;

  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao registrar doação');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/doacao']);
  }

}
