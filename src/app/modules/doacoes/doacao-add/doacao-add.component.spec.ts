import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoacaoAddComponent } from './doacao-add.component';

describe('DoacaoAddComponent', () => {
  let component: DoacaoAddComponent;
  let fixture: ComponentFixture<DoacaoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoacaoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoacaoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
