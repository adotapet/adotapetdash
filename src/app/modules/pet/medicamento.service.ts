import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';
import { Medicamento } from '../../models/medicamento.model';


@Injectable()
export class MedicamentoService {
  public medicamento: any;
  public filter = { };

  constructor(private authHttp: AuthHttp) {
  }

  public create(medicamento: Medicamento): any {
    delete medicamento._id;
    return this.authHttp.post('/medicamento', medicamento);
  }

  public edit(medicamento: Medicamento): any {
    return this.authHttp.put(`/medicamento/${medicamento._id}`, medicamento);
  }

  public read(id) {
    return this.authHttp.get('/medicamento/');
  }

  public getAll() {
    return this.authHttp.get('/medicamento');
  }

  public search(filtro: any) {
    return new Promise((resolve, reject) => {
      this.getAll().then((result: any) => {
        let medicamentosMatch = result.filter((elem) => {
          return  filtro.nome ? elem.nome.toLowerCase().indexOf(filtro.nome.toLowerCase()) != -1 : true &&
                  filtro['endereco.cidade'] ? elem.endereco.cidade === filtro['endereco.cidade'] : true &&
                  filtro['endereco.uf'] ? elem.endereco.uf === filtro['endereco.uf'] : true
         });
        resolve(medicamentosMatch);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  public setSearch(parameters) {
    this.filter = parameters;
  }

  public getSearch() {
    return this.filter;
  }

  public popularMedicamentos(medicamentos: any[]): Promise<any> {
    return new Promise((res, rej) => {
      let populated = [];
      this.getAll().then((result: any) => {
        medicamentos.forEach((funcID: any) => {
          const finded = result.find((func: any) => func._id === funcID);
          finded ? populated.push(finded) : null;
        });
        res(populated);
      }).catch(err => rej(err));
    });
  }

}
