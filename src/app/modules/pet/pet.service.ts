import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';
import { Pet } from '../../models/pet.model';
import { AbrigoService } from '../abrigo/abrigo.service';
import { LocalStorageService } from '../../services/local-storage.service';


@Injectable()
export class PetService {
  public pet: any;
  public filter = { };
  public readingPet: any;
  constructor(private authHttp: AuthHttp, private abrigoService: AbrigoService, private localStorage: LocalStorageService) {
  }

  public create(pet: Pet) {
    return this.authHttp.post('/pet', pet);
  }

  public edit(pet: Pet) {
    return this.authHttp.put(`/pet/${pet._id}`, pet);
  }

  public read(id) {
    return this.authHttp.get('/pet/');
  }

  public getOnebyId(id) {
    return this.authHttp.get('/pet/' + id);
  }

  public delete(pet: Pet) {
    return this.authHttp.delete(`/pet/${pet._id}`);
  }

  public getAll() {
    return this.authHttp.get('/pet');
  }

  public search(filtro: any, ignoreAbrigo: boolean = true) {
    return new Promise((resolve, reject) => {
      this.getAll().then((result: any) => {
        let petsMatch = result.filter((elem) => {
          return  (filtro.nome ? elem.nome.toLowerCase().indexOf(filtro.nome.toLowerCase()) != -1 : true) &&
                  (filtro.especie ? elem.especie.toLowerCase().indexOf(filtro.especie.toLowerCase()) != -1 : true) &&
                  (filtro.raca ? elem.raca.toLowerCase().indexOf(filtro.raca.toLowerCase()) != -1 : true) &&
                  (filtro.porte ? elem.porte.toLowerCase().indexOf(filtro.porte.toLowerCase()) != -1 : true) &&
                  (ignoreAbrigo ? true : elem.abrigo._id === this.localStorage.getAbrigoLogged()._id);
         });
         console.log(petsMatch);
        resolve(petsMatch);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  public setSearch(parameters) {
    this.filter = parameters;
  }

  public getSearch() {
    return this.filter;
  }

  public getPetsFromAbrigo(abrigoID: string): Promise<any> {
    return new Promise((res, rej) => {
      this.getAll().then((result: any) => {
        const petfided = result.filter(pet => pet.abrigo._id === abrigoID);
        res(petfided ? petfided : null);
      }).catch(err => rej(err));
    });
  }
}
