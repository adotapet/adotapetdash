import { Component, OnInit } from '@angular/core';
import { Pet, Alimentacao } from '../../../models/pet.model';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';
import { NavService } from '../../../services/navigation.service';
import { PetService } from '../pet.service';
import { AbrigoService } from '../../abrigo/abrigo.service';
import { LocalStorageService } from '../../../services/local-storage.service';
import { MaskService } from '../../../core/mask.service';
import { VacinaService } from '../vacina.service';
import { MedicamentoService } from '../medicamento.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { AuthHttp } from '../../../services/auth.http.service';
import { DomSanitizer } from '../../../../../node_modules/@angular/platform-browser';

@Component({
  selector: 'app-pet-edit',
  templateUrl: './pet-edit.component.html',
  styleUrls: ['./pet-edit.component.scss']
})
export class PetEditComponent implements OnInit {
  model: Pet = new Pet();

  form: FormGroup;
  booleanValues = Pet.BOOLEAN_VALUES;
  sexoOptions = Pet.SEXO;
  porteOptions = Pet.PORTE;
  caracteristicas: any = {};

  mask:any = {};
  racasOptions = [];
  blobPerfil: any;

  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService,
    private nav: NavService,
    private service: PetService,
    private abrigoService: AbrigoService,
    private localStorage: LocalStorageService,
    private maskService: MaskService,
    private vacinaService: VacinaService,
    private medicamentoService: MedicamentoService,
    private commonFunctions: CommonFunctionsService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer
  ) {
    if (!this.nav.getParameters('model')) {
      this.list();
      return;
    }
    this.model.fromApi(this.nav.getParameters('model'));
  }

  async ngOnInit() {
    this.model.vacinacoes = await this.vacinaService.popularVacinas(this.model.vacinacoes);
    this.model.medicamentosEspecificos = await this.medicamentoService.popularMedicamentos(this.model.medicamentosEspecificos);
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
    console.log(this.form);
    this.initCaracteristicas();
    this.initMask();
    this.updateRacas(this.model.especie, true);
    this.carregarFoto();
  }

  carregarFoto() {
    if (this.model.fotoPerfil) {
      const loading = this.toast.info('Carregando foto....');
      this.http.getFile(this.model.fotoPerfil).then(result => {
        this.blobPerfil = result;
        this.toast.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toast.clear(loading.toastId);
      });
    }
  }

  initMask() {
    this.mask.data = this.maskService.dateMask;
  }

  updateRacas(especie, firstUpdate = false) {
    this.racasOptions = [];
    firstUpdate ? null : this.form.patchValue({raca: null});
    if (especie === 'Gato') {
      this.commonFunctions.getRacasGato().subscribe(result => {
        this.racasOptions = result.racas;
      });
    } else if (especie === 'Cachorro') {
      this.commonFunctions.getRacasCachorro().subscribe(result => {
        this.racasOptions = result.racas;
      });      
    }
  }

  initCaracteristicas() {
    this.caracteristicas['grauAmizadeComAnimais'] = this.model.caracteristicas.grauAmizadeComAnimais;
    this.caracteristicas['grauAmizadoComCriancas'] = this.model.caracteristicas.grauAmizadoComCriancas;
    this.caracteristicas['grauAmizadeComDesconhecidos'] = this.model.caracteristicas.grauAmizadeComDesconhecidos;
    this.caracteristicas['grauBrincalhao'] = this.model.caracteristicas.grauBrincalhao;
    this.caracteristicas['grauEnergia'] = this.model.caracteristicas.grauEnergia;
    this.caracteristicas['grauProtecao'] = this.model.caracteristicas.grauProtecao;
    this.caracteristicas['grauAgressividade'] = this.model.caracteristicas.grauAgressividade;
    this.caracteristicas['grauFobiaAoRuido'] = this.model.caracteristicas.grauFobiaAoRuido;
  }

  addVacina() {
    const formBuilder = new FormBuilder();
    let vacinas = this.form.get('vacinacoes') as FormArray;
    vacinas.push(formBuilder.group(new Vacina().toFormGroup()));
  }

  addMedicamento() {
    const formBuilder = new FormBuilder();
    let medicamentos = this.form.get('medicamentosEspecificos') as FormArray;
    medicamentos.push(formBuilder.group(new Medicamento().toFormGroup()));
  }

  addAlimento() {
    const formBuilder = new FormBuilder();
    let alimentacao = this.form.get('alimentacoesEspecificas') as FormArray;
    alimentacao.push(formBuilder.group(new Alimentacao().toFormGroup()));
  }

  changeCaracteristica(caracterisca, value) {
    this.caracteristicas[caracterisca] = value;
    console.log(this.caracteristicas);
  }

  removeInForm(formName, index) {
    let form = this.form.get(formName) as FormArray;
    form.removeAt(index);
  }

  async save() {
    if (this.form.valid) {
      this.model.fromFormGroup(this.form);
      this.model.caracteristicas = this.caracteristicas;
      const loading = this.toast.info('Carregando....');
      this.model.vacinacoes = await this.saveArray(this.model.vacinacoes, this.vacinaService);
      this.model.medicamentosEspecificos = await this.saveArray(this.model.medicamentosEspecificos, this.medicamentoService);
      const abrigo = this.localStorage.getAbrigoLogged();
      const foto = await this.saveFoto();
      this.model.fotoPerfil = foto ? foto._id : null;
      this.service.edit(this.model.toApi(abrigo._id)).then((result: any) => {
        console.log(result);
        this.toast.clear(loading.toastId);
        this.toast.success('Edição realizada com sucesso!');
        this.list();
      }).catch((err) => {
        this.toast.clear(loading.toastId);
        console.log(err);
        alert('Dados inválidos');
      });
    } else {
      this.toast.error('Formulário inválido!');
      console.log(this.form);
    }

    this.model.fromFormGroup(this.form);
    // this.model.caracteristicas = this.caracteristicas;
    this.formValidate.validateAllFormFields(this.form);
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  saveFoto(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.blobPerfil) {
        resolve(await this.http.uploadFile(this.blobPerfil));
      } else {
        resolve(null);
      }
    });
  }

  uploadFile(event) {
    let file = event.target.files[0];

    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      this.blobPerfil = new Blob([reader.result], {type: 'application/octet-stream'});
      console.log(this.blobPerfil);
      // this.http.uploadFile(blob).then((data: any) => {
      //   this.source = this.sanitizer.bypassSecurityTrustUrl(data);
      // });
    }
  }

  saveArray(array, service): Promise<any[]> {
    return new Promise((resolve, reject) => {
      let promises = [];
      let ids = [];
      for (let item of array) {
        if (!item._id) {
          promises.push(service.create(item));
        } else {
          promises.push(service.edit(item));
        }
      }
      Promise.all(promises).then(result => {
        result.forEach(el => ids.push(el._id));
        resolve(ids);
      });
    });
  }

  list() {
    this.router.navigate(['/dash/pet']);
  }

}
