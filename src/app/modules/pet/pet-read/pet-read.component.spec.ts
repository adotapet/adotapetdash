import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetReadComponent } from './pet-read.component';

describe('PetReadComponent', () => {
  let component: PetReadComponent;
  let fixture: ComponentFixture<PetReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
