import { NavService } from './../../../services/navigation.service';
import { Component, OnInit } from '@angular/core';
import { Pet } from '../../../models/pet.model';
import { Router } from '@angular/router';
import { ToastrService } from '../../../../../node_modules/ngx-toastr';
import { AuthHttp } from '../../../services/auth.http.service';
import { DomSanitizer } from '../../../../../node_modules/@angular/platform-browser';
import { VacinaService } from '../vacina.service';
import { MedicamentoService } from '../medicamento.service';

@Component({
  selector: 'app-pet-read',
  templateUrl: './pet-read.component.html',
  styleUrls: ['./pet-read.component.scss']
})
export class PetReadComponent implements OnInit {
  model: Pet = new Pet();
  visitante = false;
  blobPerfil: any;
  constructor(
    private nav: NavService,
    private router: Router,
    private toast: ToastrService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer,
    private vacinaService: VacinaService,
    private medicamentoService: MedicamentoService
  ) {
    if (!this.nav.getParameters('model')) {
      this.list();
      return;
    }
    this.model.fromApi(this.nav.getParameters('model'));
    console.log(this.model);
   }

  async ngOnInit() {    
    this.carregarFoto();
    this.model.vacinacoes = await this.vacinaService.popularVacinas(this.model.vacinacoes);
    this.model.medicamentosEspecificos = await this.medicamentoService.popularMedicamentos(this.model.medicamentosEspecificos);
  }

  carregarFoto() {
    if (this.model.fotoPerfil) {
      const loading = this.toast.info('Carregando foto....');
      this.http.getFile(this.model.fotoPerfil).then(result => {
        this.blobPerfil = result;
        this.toast.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toast.clear(loading.toastId);
      });
    }
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  list() {
    this.router.navigate(['/dash/pet']);
  }

}
