import { MedicamentoService } from './../medicamento.service';
import { Component, OnInit } from '@angular/core';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { Pet, Alimentacao } from '../../../models/pet.model';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';
import { MaskService } from '../../../core/mask.service';
import { PetService } from '../pet.service';
import { AbrigoService } from '../../abrigo/abrigo.service';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Abrigo } from '../../../models';
import { VacinaService } from '../vacina.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { AuthHttp } from '../../../services/auth.http.service';

import { DomSanitizer } from '@angular/platform-browser';
import { MessagesCore } from '../../../shared/class/messagesCore.class';

@Component({
  selector: 'app-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.scss']
})
export class PetAddComponent implements OnInit {
  model: Pet = new Pet();

  form: FormGroup;
  booleanValues = Pet.BOOLEAN_VALUES;
  sexoOptions = Pet.SEXO;
  porteOptions = Pet.PORTE;
  caracteristicas: any = {};
  racasOptions: any[] = [];

  mask: any = {};
  fotoFile: any;
  source: any = '../../../../assets/dog-icon.png';
  blobPerfil: any;

  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService,
    private maskService: MaskService,
    private service: PetService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer,
    private abrigoService: AbrigoService,
    private localStorage: LocalStorageService,
    private vacinaService: VacinaService,
    private medicamentoService: MedicamentoService,
    private commonFunctions: CommonFunctionsService
  ) { }

  ngOnInit() {
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
    this.initCaracteristicas();
    this.initMask();
    // this.getFile();
  }

  updateRacas(especie) {
    this.racasOptions = [];
    this.form.patchValue({raca: null});
    if (especie === 'Gato') {
      this.commonFunctions.getRacasGato().subscribe(result => {
        this.racasOptions = result.racas;
      });
    } else if (especie === 'Cachorro') {
      this.commonFunctions.getRacasCachorro().subscribe(result => {
        this.racasOptions = result.racas;
      });
    }
  }

  initMask() {
    this.mask.data = this.maskService.dateMask;
  }

  initCaracteristicas() {
    this.caracteristicas['grauAmizadeComAnimais'] = 0;
    this.caracteristicas['grauAmizadoComCriancas'] = 0;
    this.caracteristicas['grauAmizadeComDesconhecidos'] = 0;
    this.caracteristicas['grauBrincalhao'] = 0;
    this.caracteristicas['grauEnergia'] = 0;
    this.caracteristicas['grauProtecao'] = 0;
    this.caracteristicas['grauAgressividade'] = 0;
    this.caracteristicas['grauFobiaAoRuido'] = 0;
  }

  addVacina() {
    const formBuilder = new FormBuilder();
    let vacinas = this.form.get('vacinacoes') as FormArray;
    vacinas.push(formBuilder.group(new Vacina().toFormGroup()));
  }

  addMedicamento() {
    const formBuilder = new FormBuilder();
    let medicamentos = this.form.get('medicamentosEspecificos') as FormArray;
    medicamentos.push(formBuilder.group(new Medicamento().toFormGroup()));
  }

  addAlimento() {
    const formBuilder = new FormBuilder();
    let alimentacao = this.form.get('alimentacoesEspecificas') as FormArray;
    alimentacao.push(formBuilder.group(new Alimentacao().toFormGroup()));
  }

  changeCaracteristica(caracterisca, value) {
    this.caracteristicas[caracterisca] = value;
  }

  removeInForm(formName, index) {
    let form = this.form.get(formName) as FormArray;
    form.removeAt(index);
  }

  async save() {
    if (this.form.valid) {
      this.model.fromFormGroup(this.form);
      this.model.caracteristicas = this.caracteristicas;
      const loading = this.toast.info('Carregando...');
      this.model.vacinacoes = await this.saveArray(this.model.vacinacoes, this.vacinaService);
      this.model.medicamentosEspecificos = await this.saveArray(this.model.medicamentosEspecificos, this.medicamentoService);
      const foto = await this.saveFoto();
      this.model.fotoPerfil = foto ? foto._id : null;
      const abrigo = this.localStorage.getAbrigoLogged();
      this.service.create(this.model.toApi(abrigo._id)).then((res: any) => {
        if (this.formValidate.checkResultError(res)) {
          throw new Error(res.message);
        }
        abrigo.pets.push(res._id);
        return this.abrigoService.edit(abrigo);
      })
      .then((res: any) => {
        if (this.formValidate.checkResultError(res)) {
          throw new Error(res.message);
        }
        this.toast.clear(loading.toastId);
        this.toast.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesAdd);
        this.list();
      })
      .catch((err) => {
        console.log(err);
        this.toast.clear(loading.toastId);
        this.toast.error(MessagesCore.tituloError, err);
      });
    } else {
      this.toast.error('Formulário inválido!');
      console.log(this.form);
    }

    // this.model.fromFormGroup(this.form);
    // this.formValidate.validateAllFormFields(this.form);
  }

  saveFoto(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.blobPerfil) {
        resolve(await this.http.uploadFile(this.blobPerfil));
      } else {
        resolve(null);
      }
    });
  }

  saveArray(array, service): Promise<any[]> {
    return new Promise((resolve, reject) => {
      let promises = [];
      let ids = [];
      for (let item of array) {
        if (!item._id) {
          promises.push(service.create(item));
        } else {
          promises.push(service.edit(item));
        }
      }
      Promise.all(promises).then(result => {
        result.forEach(el => ids.push(el._id));
        resolve(ids);
      });
    });
  }

  disableForm(value, formControlName) {
    if (value.id) {
      this.form.get(formControlName).enable();
    } else {
      this.form.get(formControlName).disable();
    }
  }

  list() {
    this.router.navigate(['/dash/pet']);
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  // 5bbbf4924e27870021e08af9
  uploadFile(event) {
    let file = event.target.files[0];

    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      this.blobPerfil = new Blob([reader.result], {type: 'application/octet-stream'});
      console.log(this.blobPerfil);
      // this.http.uploadFile(blob).then((data: any) => {
      //   this.source = this.sanitizer.bypassSecurityTrustUrl(data);
      // });
    }
  }

  async getFile() {
    // this.http.getAll(); 5bbc00d54e27870021e08b03
    this.http.getFile('5bbc00d54e27870021e08b03').then((img: any) =>{
      // this.source = blob;
      console.log('result', img);
        this.source = img;
        // this.source = this.sanitizer.bypassSecurityTrustUrl(blob);
      // this.source = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(blob));
    }).catch((err) => {
      console.log(err);
    });
  }


}
