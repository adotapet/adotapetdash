import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PetService } from '../pet.service';
import { Pet } from '../../../models/pet.model';
import { NavService } from '../../../services/navigation.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from '../../../services/local-storage.service';
import { AlertsService } from '../../../services/alerts.service';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.scss']
})
export class PetListComponent implements OnInit {
  models:any = [];

  nomeInput: string = '';
  especieInput: string = 'Todos';
  racaInput: string = '';
  porteInput: string = 'Todos';

  filtro : any = {};
  constructor(
    private router: Router,
    private petService: PetService,
    private nav: NavService,
    private toast: ToastrService,
    private localStorage: LocalStorageService,
    private alertsService: AlertsService
  ) { }

  ngOnInit() {
    this.all();
  }

  async all() {
    const loading = this.toast.info('Carregando');
    // let result: any = await this.petService.getAll();
    // this.models = result.filter(el => el.abrigo._id === this.localStorage.getAbrigoLogged()._id);
    // this.models = result.filter((elem) => {
    //   return elem.abrigo && elem.abrigo._id === this.localStorage.getAbrigoLogged()._id;
    // });
    this.models = await this.petService.getPetsFromAbrigo(this.localStorage.getAbrigoLogged()._id);
    this.toast.clear(loading.toastId);
  }

  add() {
    this.router.navigate(['dash/pet/add']);
  }

  edit(model) {
		this.nav.clearParameters();
		this.nav.setParameters('model', model);
		this.router.navigate(['/dash/pet/edit']);
  }

  read(model) {
		this.nav.clearParameters();
		this.nav.setParameters('model', model);
		this.router.navigate(['/dash/pet/read']);
  }

  confirmDelete(model) {
    this.alertsService.confirmAlert(this.delete.bind(this, model));
  }

  confirmAdocao(model, trocarEstadoPara) {
    if (trocarEstadoPara === 'Não Disponível para Adoação') {
      this.alertsService.confirmAlert(this.changeEstado.bind(this, model, trocarEstadoPara), 'Deseja retirar o Pet da adoção?');
    } else {
      this.alertsService.confirmAlert(this.changeEstado.bind(this, model, trocarEstadoPara), 'Deseja colocar o Pet para adoção?');
    }
  }

  changeEstado(model, estado) {
    model.adocao.status = estado;
    this.petService.edit(model).then(() => {
      if (estado === 'Não Disponível para Adoação') {
        this.toast.success('Pet retirado da adoção!');
      } else {
        this.toast.success('Pet colocado para adoção!');
      }
      this.all();
    });
  }

  delete(model) {
    this.petService.delete(model).then(() => {
      this.toast.success('Registro deletado');
      this.all();
    });
  }

  applyFilter() {
    this.nomeInput ? this.filtro.nome = this.nomeInput : delete this.filtro.nome;
    this.racaInput ? this.filtro.raca = this.racaInput : delete this.filtro.raca;
    this.especieInput !== 'Todos' ? this.filtro.especie = this.especieInput : delete this.filtro.especie;
    this.porteInput !== 'Todos' ? this.filtro.porte = this.porteInput : delete this.filtro.porte;
    this.search();
  }

  search() {
    this.petService.search(this.filtro, false).then((res: any[]) => {
      this.models = res;
    }).catch((err) => {
      console.log(err);
    });
  }

  limparFiltros() {
    this.nomeInput = '';
    this.racaInput = '';
    this.especieInput = 'Todos';
    this.porteInput = 'Todos';
    this.filtro['pet.abrigo'] = this.localStorage.getAbrigoLogged()._id;
    this.applyFilter();
  }
}
