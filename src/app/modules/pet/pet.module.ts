import { MedicamentoService } from './medicamento.service';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetAddComponent } from './pet-add/pet-add.component';
import { Routes, RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { TabsModule } from 'ngx-bootstrap';
import { PetListComponent } from './pet-list/pet-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { PetEditComponent } from './pet-edit/pet-edit.component';
import { PetReadComponent } from './pet-read/pet-read.component';
import { TextMaskModule } from 'angular2-text-mask';
import { MaskService } from '../../core/mask.service';
import { PetService } from './pet.service';
import { AbrigoService } from '../abrigo/abrigo.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { NavService } from '../../services/navigation.service';
import { VacinaService } from './vacina.service';
import { CommonFunctionsService } from '../../services/common-functions.service';
import { AlertsService } from '../../services/alerts.service';

const routes: Routes = [
  { path: '', redirectTo: 'list'},
  { path: 'list', component: PetListComponent },
  { path: 'add', component: PetAddComponent },
  { path: 'edit', component: PetEditComponent },
  { path: 'read', component: PetReadComponent },
  // { path: 'edit', loadChildren: './abrigo-edit/abrigo-edit.module#AbrigoEditModule' },
  // { path: 'consultorio', loadChildren: './modules/consultorio/consultorio.module#ConsultorioModule' },
  // { path: 'medicos', loadChildren: './modules/medico/medico.module#MedicoModule'}
];

@NgModule({
  imports: [
    TextMaskModule,
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot()
  ],
  declarations: [PetAddComponent, PetListComponent, PetEditComponent, PetReadComponent],
  providers: [
    FormCustomValidator,
    ToastrService,
    MaskService,
    PetService,
    AbrigoService,
    LocalStorageService,
    NavService,
    VacinaService,
    MedicamentoService,
    CommonFunctionsService,
    AlertsService
  ]
})
export class PetModule {
  static forRoot(): ModuleWithProviders {
      return {
        ngModule: PetModule,
        providers: [ PetService ]
      }
  }
}
