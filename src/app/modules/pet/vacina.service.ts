import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';
import { Vacina } from '../../models/vacina.model';


@Injectable()
export class VacinaService {
  public vacina: any;
  public filter = { };

  constructor(private authHttp: AuthHttp) {
  }

  public create(vacina: Vacina): any {
    delete vacina._id;
    return this.authHttp.post('/vacina', vacina);
  }

  public edit(vacina: Vacina): any {
    return this.authHttp.put(`/vacina/${vacina._id}`, vacina);
  }

  public read(id) {
    return this.authHttp.get('/vacina/');
  }

  public getAll() {
    return this.authHttp.get('/vacina');
  }

  public search(filtro: any) {
    return new Promise((resolve, reject) => {
      this.getAll().then((result: any) => {
        let vacinasMatch = result.filter((elem) => {
          return  filtro.nome ? elem.nome.toLowerCase().indexOf(filtro.nome.toLowerCase()) != -1 : true &&
                  filtro['endereco.cidade'] ? elem.endereco.cidade === filtro['endereco.cidade'] : true &&
                  filtro['endereco.uf'] ? elem.endereco.uf === filtro['endereco.uf'] : true
         });
        resolve(vacinasMatch);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  public setSearch(parameters) {
    this.filter = parameters;
  }

  public getSearch() {
    return this.filter;
  }

  public popularVacinas(vacinas: any[]): Promise<any> {
    return new Promise((res, rej) => {
      let populated = [];
      this.getAll().then((result: any) => {
        vacinas.forEach((funcID: any) => {
          const finded = result.find((func: any) => func._id === funcID);
          finded ? populated.push(finded) : null;
        });
        res(populated);
      }).catch(err => rej(err));
    });
  }

}
