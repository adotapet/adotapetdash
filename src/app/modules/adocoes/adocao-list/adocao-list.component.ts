import { Component, OnInit } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';
import { Router } from '@angular/router';
import { NavService } from '../../../services/navigation.service';
import { SolicitacaoService } from '../services/solicitacao.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { PetService } from '../../pet/pet.service';

@Component({
  selector: 'app-adocao-list',
  templateUrl: './adocao-list.component.html',
  styleUrls: ['./adocao-list.component.scss']
})
export class AdocaoListComponent implements OnInit {
  adocao = new Solicitacao();
  models: any = [];

  private nomePetInput = '';
  private nomeSolicitanteInput = '';
  private statusInput = 'Todos';

  constructor(
    public router:Router,
    public nav: NavService,
    private solicitacaoService: SolicitacaoService,
    private toast: ToastrService,
    private petService: PetService,
    private localStorage: LocalStorageService
    ) { }

  ngOnInit() {
    this.search();
  }

  search() {
    const loading = this.toast.info('Carregando...');
    this.solicitacaoService.getAll().then((result: any) => {
      this.toast.clear(loading.toastId);
      this.models = result.filter((sol) => {
        return  (sol.pet.abrigo === this.localStorage.getAbrigoLogged()._id && sol.adotante) &&
                (this.nomePetInput === '' ? true : (sol.pet.nome.toLowerCase().indexOf(this.nomePetInput.toLowerCase()) !== -1)) &&
                (this.nomeSolicitanteInput === '' ? true : (sol.adotante.nome.toLowerCase().indexOf(this.nomeSolicitanteInput.toLowerCase()) !== -1)) &&
                (this.statusInput === 'Todos' ? true : (sol.status === this.statusInput));
      });
      this.models = this.models.reverse();
      console.log(this.models);
    }).catch(err => {
      this.toast.clear(loading.toastId);
      console.log(err);
    });
  }

  returnSituacao(i) {
    if (i < 3) {
      return 'Pendente';
    }
    if (i >= 3 && i < 5) {
      return 'Aceita';
    }
    if (i >= 5) {
      return 'Recusada';
    }
  }

  read(model) {
    this.nav.clearParameters();
    this.nav.setParameters('model', model);
    this.router.navigate(['dash/adocao/read']);
  }

  recusar(model, motivo) {
    const loading = this.toast.info('Carregando');
    model.status = 'Recusada';
    model.motivo = motivo;
    console.log(model);
    this.solicitacaoService.edit(model).then(() => {
      this.toast.success('Solicitação recusada com sucesso!');
    }).catch((err) => {
      model.status = 'Em Espera';
      this.toast.error('Não foi possível recusar a solicitação!');
      console.log(err);
    });
  }

  aprovar(model) {
    const loading = this.toast.info('Carregando');
    const backup = Object.assign({}, model.pet);
    model.status = 'Aceita';
    this.solicitacaoService.edit(model).then(() => {
      model.pet.adocao.status = 'Adotado';
      return this.petService.edit(model.pet);
    }).then(() => {
      this.toast.success('Solicitação aprovada com sucesso!');
    }).catch((err) => {
      model.pet.estado = backup.estado;
      model.status = 'Em Espera';
      this.toast.error('Não foi possível aprovar a solicitação!');
      console.log(err);
    });
  }

  limparFiltros() {
    this.nomePetInput = '';
    this.nomeSolicitanteInput = '';
    this.statusInput = 'Todos';
    this.search();
  }

}
