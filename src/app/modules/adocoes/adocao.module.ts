import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdocaoAddComponent } from './adocao-add/adocao-add.component';
import { Routes, RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { TabsModule } from 'ngx-bootstrap';
import { AdocaoListComponent } from './adocao-list/adocao-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdocaoEditComponent } from './adocao-edit/adocao-edit.component';
import { AdocaoReadComponent } from './adocao-read/adocao-read.component';
import { SolicitacaoService } from './services/solicitacao.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { PetService } from '../pet/pet.service';
import { RecusarModalModule } from '../../shared/modules/recusar-modal/recusar-modal.module';

const routes: Routes = [
  { path: '', redirectTo: 'list'},
  { path: 'list', component: AdocaoListComponent },
  { path: 'add', component: AdocaoAddComponent },
  { path: 'edit', component: AdocaoEditComponent },
  { path: 'read', component: AdocaoReadComponent },
  // { path: 'edit', loadChildren: './abrigo-edit/abrigo-edit.module#AbrigoEditModule' },
  // { path: 'consultorio', loadChildren: './modules/consultorio/consultorio.module#ConsultorioModule' },
  // { path: 'medicos', loadChildren: './modules/medico/medico.module#MedicoModule'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    RecusarModalModule
  ],
  declarations: [AdocaoAddComponent, AdocaoListComponent, AdocaoEditComponent, AdocaoReadComponent],
  providers: [
    FormCustomValidator,
    ToastrService,
    SolicitacaoService,
    LocalStorageService,
    PetService
  ]
})
export class AdocaoModule { }
