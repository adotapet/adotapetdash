import { Injectable } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';
import { AuthHttp } from '../../../services/auth.http.service';
import { UsuarioService } from '../../usuario/usuario.service';

@Injectable()
export class SolicitacaoService {
  private baseUrl = '/solicitacao';
  public visitanteLogged: any;
  constructor(private authHttp: AuthHttp, public userService: UsuarioService) {
  }

  public create(solicitacao: Solicitacao) {
    let data: any = new Object();
    Object.assign(data,solicitacao);
    return this.authHttp.post(this.baseUrl, data);
  }

  public edit(solicitacao: Solicitacao) {
    let data: any = new Object();
    Object.assign(data,solicitacao);
    return this.authHttp.put(`${this.baseUrl}/${solicitacao._id}`, data);
  }

  public delete(solicitacao: any) {
    return this.authHttp.delete(`${this.baseUrl}/${solicitacao._id}`);
  }

  public getAll() {
    return this.authHttp.get(this.baseUrl);
	}

	public getOneByID(id) {
    return this.authHttp.get(this.baseUrl + '/' + id);
  }

  public read(id) {
    return this.authHttp.get(this.baseUrl);
  }

  public getSolicitacoesByVisitanteID(visitanteID) {
    return this.authHttp.get(`${this.baseUrl}?adotante=${visitanteID}`);
  }

  public getUsuarioOfAdotante(usuario_id) {
    return this.userService.getByID(usuario_id);
  }

  public getFaixasSalariais() {
    return this.authHttp.get(this.baseUrl + '/schema/');
  }
}
