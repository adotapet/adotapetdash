import { Component, OnInit, } from '@angular/core';
import { Solicitacao } from '../../../models/solicitacao.model';
import { Router } from '@angular/router';
import { NavService } from 'src/app/services/navigation.service';
import { DomSanitizer } from '../../../../../node_modules/@angular/platform-browser';
import { SolicitacaoService } from '../services/solicitacao.service';
import { AuthHttp } from '../../../services/auth.http.service';
import { ToastrService } from 'ngx-toastr';
import { PetService } from '../../pet/pet.service';
import { VisitanteService } from '../../visitante/visitante.service';

@Component({
  selector: 'app-adocao-read',
  templateUrl: './adocao-read.component.html',
  styleUrls: ['./adocao-read.component.scss']
})
export class AdocaoReadComponent implements OnInit {
  model: Solicitacao = new Solicitacao();
  visitante = false;
  blobPerfil: any;


  private faixasSalariais = [];
  constructor(public router:Router,
              public nav: NavService,
              public authHttp: AuthHttp,
              public solicitacaoService: SolicitacaoService,
              private toast: ToastrService,
              private petService: PetService,
              private visitanteService: VisitanteService,
              public http: SolicitacaoService,
              private sanitizer: DomSanitizer) {
    if (!this.nav.getParameters('model')) {
      this.list();
      return;
    }
    this.model.fromApi(this.nav.getParameters('model'));
    this.carregarFoto();

    this.solicitacaoService.getFaixasSalariais().then((res: any) => {
      console.log(res.questionario);
    })
  }

  async ngOnInit() {
    this.model.adotante = await this.visitanteService.getByID(this.model.adotante ? this.model.adotante._id || this.model.adotante : null)
  }

  list() {
    this.router.navigate(['dash/adocao/list']);
  }

  carregarFoto() {
    if (this.model.pet.fotoPerfil) {
      this.authHttp.getFile(this.model.pet.fotoPerfil).then(result => {
        this.blobPerfil = result;
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) :
    '../../../../assets/dog-icon.png';
  }

  recusar(model, motivo) {
    const loading = this.toast.info('Carregando');
    model.status = 'Recusada';
    model.motivo = motivo;
    console.log(model);
    this.solicitacaoService.edit(model).then(() => {
      this.toast.success('Solicitação recusada com sucesso!');
    }).catch((err) => {
      model.status = 'Em Espera';
      this.toast.error('Não foi possível recusar a solicitação!');
      console.log(err);
    });
  }

  aprovar(model) {
    const loading = this.toast.info('Carregando');
    const backup = Object.assign({}, model.pet);
    model.status = 'Aceita';
    this.solicitacaoService.edit(model).then(() => {
      model.pet.adocao.status = 'Adotado';
      return this.petService.edit(model.pet);
    }).then(() => {
      this.toast.success('Solicitação aprovada com sucesso!');
    }).catch((err) => {
      model.pet.estado = backup.estado;
      model.status = 'Em Espera';
      this.toast.error('Não foi possível aprovar a solicitação!');
      console.log(err);
    });
  }

}
