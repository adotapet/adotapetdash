import { Component, OnInit } from '@angular/core';
import { Adocao } from '../../../models/adocao.model';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-adocao-edit',
  templateUrl: './adocao-edit.component.html',
  styleUrls: ['./adocao-edit.component.scss']
})
export class AdocaoEditComponent implements OnInit {
  model: Adocao = new Adocao();
  form: FormGroup;

  statusOpcoes = this.model.getStatus();
  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao salvar adoção');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/adocao']);
  }

}
