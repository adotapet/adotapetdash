import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdocaoAddComponent } from './adocao-add.component';

describe('AdocaoAddComponent', () => {
  let component: AdocaoAddComponent;
  let fixture: ComponentFixture<AdocaoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdocaoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdocaoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
