import { Component, OnInit } from '@angular/core';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { Adocao } from '../../../models/adocao.model';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';

@Component({
  selector: 'app-adocao-add',
  templateUrl: './adocao-add.component.html',
  styleUrls: ['./adocao-add.component.scss']
})
export class AdocaoAddComponent implements OnInit {
  model: Adocao = new Adocao();

  form: FormGroup;
  booleanValues = Adocao.BOOLEAN_VALUES;


  private faixaSalarialOpcoes = [];
  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
  }

  save() {
    this.toast.success('Sucesso ao registrar adoção');
    this.list();
  }

  list() {
    this.router.navigate(['/dash/adocao']);
  }

}
