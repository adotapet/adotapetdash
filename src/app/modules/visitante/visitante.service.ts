import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from '../../services/auth.http.service';

import { Visitante } from '../../models/visitante.model';
import { Abrigo, Endereco } from '../../models';
import { EnderecoService } from 'src/app/services/models-services/endereco.service';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable()
export class VisitanteService {
  private baseUrl = '/visitante';
  public visitanteLogged: any;
  constructor(private authHttp: AuthHttp, private enderecoService: EnderecoService, private usuarioService: UsuarioService) {
  }

  public create(visitante: Visitante) {
    let data: any = new Object();
    Object.assign(data,visitante);
    return this.authHttp.post(this.baseUrl, data);
  }

  public edit(visitante: Visitante) {
    let data: any = new Object();
    Object.assign(data,visitante);
    return this.authHttp.put(`${this.baseUrl}/${visitante._id}`, data);
  }

  public delete(visitante: any) {
    return this.authHttp.delete(`${this.baseUrl}/${visitante._id}`);
  }

  public getByID(id: string) {
    return new Promise((res, rej) => {
      this.authHttp.get(`${this.baseUrl}/${id}`).then(async (result: any) => {
        console.log('visitanteee', result);
        // let enderecoPopulado = await this.enderecoService.getOneById(result.endereco ? result.endereco._id : result.endereco);
        // let usuarioPopulado = await this.usuarioService.getByID(result.usuario ? result.usuario._id : result.usuario);
        // result.endereco = enderecoPopulado;
        // result.usuario = usuarioPopulado;
        res(result);
      }).catch(err => rej(err));
    });
  }


  public getAll() {
    return this.authHttp.get(this.baseUrl);
  }

  public read(id) {
    return this.authHttp.get(this.baseUrl);
  }

  public getVisitanteByUserID(userID: string): Promise<any> {
    return new Promise((res, rej) => {
      this.getAll().then((result: any) => {
        const visitanteFinded = result.find(vis => vis.usuario._id === userID);
        res(visitanteFinded ? visitanteFinded : null);
      }).catch(err => rej(err));
    });
  }

}
