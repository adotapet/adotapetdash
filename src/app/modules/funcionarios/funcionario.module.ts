import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FuncionarioAddComponent } from './funcionario-add/funcionario-add.component';
import { Routes, RouterModule, Router } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { TabsModule } from 'ngx-bootstrap';
import { FuncionarioListComponent } from './funcionario-list/funcionario-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FuncionarioEditComponent } from './funcionario-edit/funcionario-edit.component';
import { FuncionarioReadComponent } from './funcionario-read/funcionario-read.component';
import { MaskService } from '../../core/mask.service';
import { TextMaskModule } from 'angular2-text-mask';
import { EnderecoService } from '../../services/models-services/endereco.service';
import { FuncionarioService } from './funcionario.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { AlertsService } from '../../services/alerts.service';

const routes: Routes = [
  { path: '', redirectTo: 'list'},
  { path: 'list', component: FuncionarioListComponent },
  { path: 'add', component: FuncionarioAddComponent },
  { path: 'edit', component: FuncionarioEditComponent },
  { path: 'read', component: FuncionarioReadComponent },
  // { path: 'edit', loadChildren: './abrigo-edit/abrigo-edit.module#AbrigoEditModule' },
  // { path: 'consultorio', loadChildren: './modules/consultorio/consultorio.module#ConsultorioModule' },
  // { path: 'medicos', loadChildren: './modules/medico/medico.module#MedicoModule'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    TextMaskModule
  ],
  declarations: [FuncionarioAddComponent, FuncionarioListComponent, FuncionarioEditComponent, FuncionarioReadComponent],
  providers: [
    FormCustomValidator,
    ToastrService,
    MaskService,
    EnderecoService,
    FuncionarioService,
    LocalStorageService,
    AlertsService
  ]
})
export class FuncionarioModule { }
