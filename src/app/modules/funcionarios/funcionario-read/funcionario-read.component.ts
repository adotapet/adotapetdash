import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../../../models/funcionario.model';
import { Router } from '@angular/router';
import { NavService } from 'src/app/services/navigation.service';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from '../../usuario/usuario.service';
import { Endereco } from 'src/app/models';

@Component({
  selector: 'app-funcionario-read',
  templateUrl: './funcionario-read.component.html',
  styleUrls: ['./funcionario-read.component.scss']
})
export class FuncionarioReadComponent implements OnInit {
  model: Funcionario = new Funcionario();
  usuario: Usuario = new Usuario();
  endereco: Endereco = new Endereco();
  visitante = false;

  acesso = { label:'Não', value: 0};
  constructor(
    private nav: NavService,
    private router: Router,
    private usuarioService: UsuarioService
  ) { 
    if (!this.nav.getParameters('model')) {
      this.list();
      return;
    }
    this.model.fromApi(this.nav.getParameters('model'));
  }

  async ngOnInit() {
    this.usuario.fromApi(this.model.usuario);
    this.endereco.fromApi(this.model.endereco);
  }


  list() {
    this.router.navigate(['/dash/funcionario']);
  }

}
