import { Injectable } from '@angular/core';
import { AuthHttp } from '../../services/auth.http.service';
import { Funcionario } from '../../models/funcionario.model';
import { error } from 'util';


@Injectable()
export class FuncionarioService {
	baseUrl = '/funcionario';
  constructor(private authHttp: AuthHttp) {
  }

  public create(funcionario: Funcionario) {
    return this.authHttp.post(this.baseUrl, funcionario);
  }

  public edit(funcionario: Funcionario) {
    return this.authHttp.put(`${this.baseUrl}/${funcionario._id}`, funcionario);
  }

  public read(id) {
    return this.authHttp.get(this.baseUrl);
	}

	public delete(funcionario: Funcionario) {
    return this.authHttp.delete(`/funcionario/${funcionario._id}`);
  }

  public getAll() {
    return this.authHttp.get(this.baseUrl);
  }

  public getFuncionarioByUserID(usuarioID: string) {
    return new Promise((res, rej) => {
      this.getAll().then((result: any) => {
        res(result.find(func => func.usuario ? func.usuario._id === usuarioID : false));
      }).catch(err => rej(error));
    });
  }

  public popularFuncionarios(funcionarios: string[]): Promise<any> {
    return new Promise((res, rej) => {
      let funcionariosPopulados = [];
      this.getAll().then((result: any) => {
        funcionarios.forEach((funcionario: any) => {
          const funcionarioFinded = result.find((func: any) => func._id === funcionario._id);
          funcionarioFinded ? funcionariosPopulados.push(funcionarioFinded) : null;
        });
        res(funcionariosPopulados);
      }).catch(err => rej(err));
    });
  }

}
