import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Funcionario } from '../../../models/funcionario.model';
import { NavService } from '../../../services/navigation.service';
import { FuncionarioService } from '../funcionario.service';
import { LocalStorageService } from '../../../services/local-storage.service';
import { Abrigo, Endereco } from '../../../models';
import { AbrigoService } from '../../abrigo/abrigo.service';
import { Alert } from 'selenium-webdriver';
import { AlertsService } from '../../../services/alerts.service';
import { Usuario } from '../../../models/usuario.model';
import { UsuarioService } from '../../usuario/usuario.service';
import { EnderecoService } from '../../../services/models-services/endereco.service';
import { ToastrService } from 'ngx-toastr';
import { MessagesCore } from '../../../shared/class/messagesCore.class';


@Component({
  selector: 'app-funcionario-list',
  templateUrl: './funcionario-list.component.html',
  styleUrls: ['./funcionario-list.component.scss']
})
export class FuncionarioListComponent implements OnInit {
  models: Funcionario[] = [];

  private filterValue = {};

  private acessoOpts =  [{label:'Sim', value: true}, {label: 'Não', value: false}];
  private admOpts =  [{label:'Sim', value: true}, {label: 'Não', value: false}];

  nomeInput = '';
  emailInput = '';
  acessoInput = null;
  admInput = null;

  constructor(
    private router: Router,
    private nav: NavService,
    private funcionarioService: FuncionarioService,
    private localStorage: LocalStorageService,
    private abrigoService: AbrigoService,
    private alertsService: AlertsService,
    private usuarioService: UsuarioService,
    private enderecoService: EnderecoService,
    private toast: ToastrService
  ) { }

  ngOnInit() {
    this.search();
  }

  search() {
    this.abrigoService.getOneByID(this.localStorage.getAbrigoLogged()._id).then(async (abrigo: any) => {

      let funcionariosPopulados = await this.funcionarioService.popularFuncionarios(abrigo.funcionarios);
      this.models = funcionariosPopulados;
      this.models = funcionariosPopulados.filter((elem) => {
        return  (this.filterValue['nomeInput']  ? elem.nome.toLowerCase().indexOf(this.filterValue['nomeInput'].toLowerCase())  : true) !== -1
      });
    }).catch((error) => {
      console.log(error);
    });
  }


  add() {
    this.router.navigate(['dash/funcionario/add']);
  }

  edit(model) {
    this.nav.clearParameters();
    this.nav.setParameters('model', model);
    this.router.navigate(['/dash/funcionario/edit']);
  }

  confirmDelete(model) {
    this.alertsService.confirmAlert(this.delete.bind(this, model));
  }

  delete(model) {
    this.usuarioService.delete({_id: model.usuario ? model.usuario._id || model.usuario : null});
    // this.enderecoService.delete(backups['endereco']).then(() => delete backups['endereco']);
    this.funcionarioService.delete(model).then(() => {
      this.search();
      this.toast.success(MessagesCore.msgSuccesDelete)
    });
  }

  read(model) {
    this.nav.clearParameters();
    this.nav.setParameters('model', model);
    this.router.navigate(['/dash/funcionario/read']);
  }

  limparFiltros() {
    this.nomeInput = '';
    // this.emailInput = '';
    // this.acessoInput = null;
    // this.admInput = null;
    this.filter();
  }

  filter() {
    this.nomeInput ? this.filterValue['nomeInput'] = this.nomeInput        : delete this.filterValue['nomeInput'];
    // this.emailInput ? this.filterValue['emailInput'] = this.emailInput     : delete this.filterValue['emailInput'];
    // this.acessoInput ? this.filterValue['acessoInput'] = this.acessoInput  : delete this.filterValue['acessoInput'];
    // this.admInput ? this.filterValue['admInput'] = this.admInput           : delete this.filterValue['admInput'];
    this.search();

  }
}
