import { Component, OnInit } from '@angular/core';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { Funcionario } from '../../../models/funcionario.model';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Vacina } from '../../../models/vacina.model';
import { Medicamento } from '../../../models/medicamento.model';
import { Endereco, Abrigo } from '../../../models';
import { MaskService } from '../../../core/mask.service';
import { Usuario } from '../../../models/usuario.model';
import { UsuarioService } from '../../usuario/usuario.service';
import { EnderecoService } from '../../../services/models-services/endereco.service';
import { FuncionarioService } from '../funcionario.service';
import { MessagesCore } from '../../../shared/class/messagesCore.class';
import { LocalStorageService } from '../../../services/local-storage.service';
import { AbrigoService } from '../../abrigo/abrigo.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';

@Component({
  selector: 'app-funcionario-add',
  templateUrl: './funcionario-add.component.html',
  styleUrls: ['./funcionario-add.component.scss']
})
export class FuncionarioAddComponent implements OnInit {
  model: Funcionario = new Funcionario();
  endereco: Endereco = new Endereco();
  usuario: Usuario = new Usuario();
  acesso = { label:'Não', value: 0};

  form: FormGroup;
  formEndereco: FormGroup;
  formUsuario: FormGroup;
  mask: any = {};

  cidades = [];
  estados = [];
  paises = [];
  constructor(
    private router: Router,
    private formValidate: FormCustomValidator,
    private toast: ToastrService,
    private common: CommonFunctionsService,
    private maskService: MaskService,
    private usuarioService: UsuarioService,
    private enderecoService: EnderecoService,
    private funcionarioService: FuncionarioService,
    private localStorage: LocalStorageService,
    private abrigoService: AbrigoService,
  ) { }

  ngOnInit() {
    let services: any = [];
    services['usuario'] = this.usuarioService;
    this.form = this.formValidate.createFormGroup(this.model.toFormGroup());
    this.formEndereco = this.formValidate.createFormGroup(this.endereco.toFormGroup());
    this.formUsuario = this.formValidate.createFormGroup(this.usuario.toFormGroup(services));
    this.initMask();

    this.common.getPaises().subscribe((result) => {
      this.paises = result.filter((elem) => elem.nome_pais === 'Brasil');
    });

    this.common.getEstados().subscribe((data) => {
      // this.jsonEstadoData = data.estados;
      this.estados = data.estados;
    });
  }

  updateCidades(estado) {
    this.cidades = estado.cidades;
  }

  initMask() {
    this.mask.email = this.maskService.emailMask;
    this.mask.cnpj = this.maskService.cnpjMask;
    this.mask.telefone = this.maskService.telefoneMask;
    this.mask.cep = this.maskService.cepMask;
  }

  save() {
    if (this.form.valid && this.formEndereco.valid && this.formUsuario.valid) {
      if (this.formUsuario.get('password').value !== this.formUsuario.get('confpassword').value) {
        this.toast.warning('As senhas digitadas não conferem!');
        return;
      }
      let backups: any = [];
      let loading = this.toast.info('Carregando...');
      this.usuario.fromFormGroup(this.formUsuario);
      this.endereco.fromFormGroup(this.formEndereco);
      this.model.fromFormGroup(this.form);
      this.usuarioService.create(this.usuario).then((result: any) => {
        backups['usuario'] = result;
        this.model.usuario = result._id;
        return this.enderecoService.create(this.endereco);
      }).then((result: any) => {
        backups['endereco'] = result;
        this.model.endereco = result._id;
        this.model.abrigo = this.localStorage.getAbrigoLogged()._id;
        return this.funcionarioService.create(this.model);
      }).then((result: any) => {
        backups['funcionario'] = result;
        let abrigo = Object.assign(new Abrigo(), this.localStorage.getAbrigoLogged());
        this.localStorage.getAbrigoLogged().funcionarios.push(result);
        abrigo.funcionarios.push(result);
        abrigo.funcionarios = abrigo.funcionarios.map(el => {
          if (el) {
            return el._id
          }
        });
        return this.abrigoService.edit(abrigo);
      }).then((result: any) => {
        console.log('resultado funcionario', result);
        this.toast.clear(loading.toastId);
        this.toast.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesAdd);
        this.list();
      }).catch(err => {
        this.deleteErros(backups);
        this.toast.clear(loading.toastId);
        this.toast.error(err);
        console.log(err);
      });

    } else {
      this.toast.error('Formulário inválido! Corrija os campos incorretos.');
      this.formValidate.validateAllFormFields(this.form);
      this.formValidate.validateAllFormFields(this.formEndereco);
      this.formValidate.validateAllFormFields(this.formUsuario);
      console.log(this.form, this.formEndereco, this.formUsuario);
    }
  }

  deleteErros(backups: any[]) {
    if (backups['usuario']) {
      this.usuarioService.delete(backups['usuario']).then(() => delete backups['usuario']);
    }
    if (backups['endereco']) {
      this.enderecoService.delete(backups['endereco']).then(() => delete backups['endereco']);
    }
    if (backups['funcionario']) {
      this.funcionarioService.delete(backups['funcionario']).then(() => delete backups['funcionario']);
    }
  }

  list() {
    this.router.navigate(['/dash/funcionario']);
  }

  atualizarNomeUsuario(event) {
    this.formUsuario.patchValue({nome: this.form.get('nome').value})
  }

}
