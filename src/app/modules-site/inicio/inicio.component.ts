import { Component, OnInit } from '@angular/core';
import { CommonFunctionsService } from '../../services/common-functions.service';
import { Router } from '@angular/router';

import { AbrigoService } from '../../modules/abrigo/abrigo.service';
import { PetService } from '../../modules/pet/pet.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  estados: any[];
  cidades: any[];

  nomeSearch: string = '';

  estadoSelected: any;
  cidadeSelected: any;

  petNomeSearch = '';
  petRacaSearch = '';
  petPorteSearch = '';


  equipeDash = [
    {
      imagem: 'https://media.licdn.com/dms/image/C4D03AQH7tdB_QaqIsQ/profile-displayphoto-shrink_200_200/0?e=1544054400&v=beta&t=4x_fWVruYf9XXWzNmtiHE_YrDxfrX18u-4W_V7P-OAI',
      nome: 'Susileia Abreu',
      cargo: 'Professora e Coordenadora',
      desc: 'Adora ser professora e curtir praia com seus filhos e seu marido. Fascinada pela área de tecnologia e inovação. Mestre em Banco de dados e redes de computadores.',
      frase: ''
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/18033892_1307280732697923_1975163289368929986_n.jpg?_nc_cat=105&_nc_ht=scontent.fvix1-1.fna&oh=5da7212b1e6d7a1e795d9eacc63d0fcb&oe=5C4185F8',
      nome: 'Anna Karinane',
      cargo: 'Scrum Master',
      desc: 'Carioca, mãe de cachorro, apaixonada por gastronomia, gamer, adoradora, interesse pela área de gestão de projetos de software e futura cientista da computação.',
      frase: 'Um pouco de ciência nos afasta de Deus. Muito, nos aproxima. - Louis Pasteur '
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/14729202_585392608252432_6529408126137095179_n.jpg?_nc_cat=100&_nc_ht=scontent.fvix1-1.fna&oh=eb6a9a6c536656da82b23039e85e6337&oe=5C874F52',
      nome: 'Edislaine Godoy',
      cargo: 'Product Owner',
      desc: "Apaixonada pela cultura coreana, don't starve gamer, interessada pela área de Inteligência Artificial e futura cientista da computação.",
      frase: '"Ensine coragem as meninas, não perfeição." - Reshma Saujani'
    },
    {
      imagem: 'https://avatars2.githubusercontent.com/u/17722397?s=400&u=d1d94b1e50f34baf4bdffd5953c2fd14bf15112c&v=4',
      nome: 'Evson Custódio',
      cargo: 'Desenvolvedor Back-End',
      desc: 'Estudante de Ciência da Computação na Universidade Vila Velha - UVV, amante da ciência e programação em geral, passa tempo jogando jogos eletrônicos e faz desafios com sigo próprio enquanto faz ciclismo',
      frase: 'A conduta define o homem - Kingsman'
    },
    {
      imagem: 'https://scontent.fbhz1-1.fna.fbcdn.net/v/t1.0-9/37925080_1702037446571302_858704369246273536_n.jpg?_nc_cat=107&_nc_ht=scontent.fbhz1-1.fna&oh=a80aa5764b361738f5e2cc4e5fb865fa&oe=5CA2A65D',
      nome: 'Caio Bassani',
      cargo: 'Analista de sistemas',
      desc: 'Trabalho na área de manutenção de sistemas e desenvolvimento de soluções SQL a 3 anos',
      frase: 'Ama-se mais o que se conquista com esforço'
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/14563402_1096605910376319_4167648295628413286_n.jpg?_nc_cat=106&_nc_ht=scontent.fvix1-1.fna&oh=d825606aa8c3ae57d37bf601358855f3&oe=5C832B61',
      nome: 'Bruno Cunha',
      cargo: 'Desenvolvedor Front',
      desc: 'Apaixonado por andar de moto, aprender novas tecnologias e, nas horas vagas, ciclista e gamer',
      frase: 'Um pouco de ciência nos afasta de Deus. Muito, nos aproxima. - Louis Pasteur'
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/16265347_1276973392389166_1312502975232361681_n.jpg?_nc_cat=101&_nc_ht=scontent.fvix1-1.fna&oh=e7cf9f091392e1760315acd3107e3b57&oe=5C512B82',
      nome: 'Daniel Freitas',
      cargo: 'Desenvolvedor Front',
      desc: 'Desenvolvedor Front e Mobile interessado em todo tipo de inovação',
      frase: 'O fracasso é inevitável'
    },
    {
      imagem: 'https://scontent.fvix2-1.fna.fbcdn.net/v/t1.0-9/10421605_926517380768935_5400975055279556200_n.jpg?_nc_cat=106&oh=4fff2617f3d1e782a9f73bf57cd465c8&oe=5C462F01',
      nome: 'Carlos Delboni',
      cargo: 'Desenvolvedor de Testes Automatizados',
      desc: 'Criativo nas horas vagas',
      frase: ''
    },
    {
      imagem: 'https://instagram.fbhz1-1.fna.fbcdn.net/vp/29414d7bd5880547acf195e58cf7b680/5CAD8513/t51.2885-19/s150x150/44828889_1886044354849167_1373995295376932864_n.jpg',
      nome: 'Diego Nascimento',
      cargo: 'Front-end Tester',
      desc: 'Gosto da área de informática, de futebol, música e etc.',
      frase: ''
    },

  ];
  private equipeMobile = [
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/10377610_669961876436109_4548561052216007862_n.jpg?_nc_cat=110&_nc_ht=scontent.fvix1-1.fna&oh=00e569c552ba304b76771f865b51f29b&oe=5C7CF0CB',
      nome: 'Marcos André',
      cargo: 'Tester',
      desc: 'Trabalha como desenvolvedor de software desde 2016. Todos seus esforços e ações estão direcionadas para a evolução, o sucesso e a felicidade das pessoas para um mundo melhor.',
      frase: ''
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/22449817_1656543684420727_3913572217153348806_n.jpg?_nc_cat=107&_nc_ht=scontent.fvix1-1.fna&oh=937e99ff6eb3a4c1818bb00646f7c237&oe=5C788804',
      nome: 'Gabriela Marques',
      cargo: 'Scrum Master',
      desc: 'Maratonista de séries, ama dormir, viajar e curtir momentos de paz.',
      frase: 'Só quero me formar!'
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/28471183_1611271018927386_8138722634889702552_n.jpg?_nc_cat=101&_nc_ht=scontent.fvix1-1.fna&oh=2e8aede34e5b2711ba13432d064997d0&oe=5C457BC0',
      nome: 'Thaynan Saveli',
      cargo: 'Design',
      desc: 'Gosto de música, de viajar,  jogador de LoL, e design gráfico',
      frase: 'Baguncinha, ação e diversão. Mike da SWAT'
    },
    {
      imagem: 'https://instagram.fvix2-2.fna.fbcdn.net/vp/d9dec340e3b4d884ae8d7e8519563a35/5C539B52/t51.2885-19/s150x150/40593851_1646376905473977_1265635628435898368_n.jpg',
      nome: 'Pedro Mariani',
      cargo: 'Engenheiro de Software',
      desc: 'Graduando em ciência da computação, estagiário na Tecnologia Operacional da Vale, namorado, pai adotivo do Cocaína, fã de Led Zeppelin, super-heróis e vídeo-games.',
      frase: 'Do or do not, there is no try.'
    },
    {
      imagem: 'https://scontent.fvix2-1.fna.fbcdn.net/v/t1.0-9/fr/cp0/e15/q65/43112374_1871242169625482_2355564286526357504_n.jpg?_nc_cat=105&efg=eyJpIjoidCJ9&_nc_ht=scontent.fvix2-1.fna&oh=7c901e56efa555fd9c5071d4533116ed&oe=5C44D04C',
      nome: 'Kelvin Xisto',
      cargo: 'Software Developer',
      desc: 'Aficionado por tecnologia, gamer e apaixonado pelo desenvolvimento de software. Não recuso uma cerveja ou uma partida de LoL/CS',
      frase: 'Todo dia é um novo dia, uma nova oportunidade de se tornar a pessoa que realmente quer ser e criar a vida que quer.'
    },
    {
      imagem: 'https://instagram.fvix2-2.fna.fbcdn.net/vp/425c994e08b7672f68565910366a77f7/5C7080EF/t51.2885-15/e35/43778394_578707822549233_1126091498393911566_n.jpg',
      nome: 'Gabriel Schmidt',
      cargo: 'Programador',
      desc: 'Gosto de sair com meus amigos pra beber, músicas e tatuagens.',
      frase: 'Estamos anos luz daquele lugar.'
    },
    {
      imagem: 'https://scontent.fvix1-1.fna.fbcdn.net/v/t1.0-9/43085698_10155988851066173_4186083896451923968_n.jpg?_nc_cat=100&_nc_ht=scontent.fvix1-1.fna&oh=532d1ee8ab38fe66327439a779e765f2&oe=5C511F6E',
      nome: 'Ronaldo Cohin',
      cargo: 'Testador',
      desc: 'Entusiasta de Inovação que acredita que a tecnologia pode mudar a vida das pessoas.',
      frase: 'Se a opção for a catástrofe, nada é impossível.'
    },
    {
      imagem: 'https://scontent.fbhz1-1.fna.fbcdn.net/v/t1.0-1/17342947_1117111418399991_3365547483568881599_n.jpg?_nc_cat=100&_nc_ht=scontent.fbhz1-1.fna&oh=4e1b926a772024d13b65dfb8ff031c18&oe=5C792DE6',
      nome: 'Gabriel da Silva',
      cargo: 'Designer',
      desc: 'Um cara legal',
      frase: ' Mano eu tenho mt coisa pra fazer, to perdidão kk'
    }
  ]
  constructor(
    private common: CommonFunctionsService,
    private router: Router,
    private abrigoService: AbrigoService,
    private petService: PetService
  ) {
   }

  ngOnInit() {
    this.loadEstados();
  }

  loadEstados() {
    this.common.getEstados().subscribe(result => {
      this.estados = result.estados;
    });
  }

  updateCidades(estado) {
    this.cidadeSelected = null;
    this.cidades = estado ? estado.cidades : [];
  }

  searchAbrigo() {
    let filtro = {};
    this.nomeSearch ? filtro['nome'] = this.nomeSearch : delete filtro['nome'];
    this.estadoSelected ? filtro['estado'] = this.estadoSelected : delete filtro['estado'];
    this.cidadeSelected ? filtro['cidade'] = this.cidadeSelected : delete filtro['cidade'];
    this.abrigoService.setSearch(filtro);
    this.router.navigate(['/search/abrigo']);
  }

  searchPet() {
    let filtro = {};
    this.petNomeSearch ? filtro['nome'] = this.petNomeSearch : delete filtro['nome'];
    this.petRacaSearch ? filtro['raca'] = this.petRacaSearch : delete filtro['raca'];
    this.petPorteSearch ? filtro['porte'] = this.petPorteSearch : delete filtro['porte'];
    this.petService.setSearch(filtro);
    this.router.navigate(['/search/pet']);
  }

}
