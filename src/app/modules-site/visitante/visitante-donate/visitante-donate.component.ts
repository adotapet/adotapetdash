import {Component, Inject, OnInit} from '@angular/core';
import {Visitante} from 'src/app/models/visitante.model';
import {VisitanteService} from 'src/app/modules/visitante/visitante.service';
import {LocalStorageService} from 'src/app/services/local-storage.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthHttp} from 'src/app/services/auth.http.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {NavService} from 'src/app/services/navigation.service';
import {AlertsService} from "../../../services/alerts.service";
import {Abrigo} from "../../../models";
import {FormGroup} from "@angular/forms";
import {FormCustomValidator} from "../../../services/form-custom-validator.service";
import {Doacao} from "../../../models/doacao.model";
import {SESSION_STORAGE, StorageService} from "angular-webstorage-service";
import swal from 'sweetalert2';
import {AbrigoService} from "../../../modules/abrigo/abrigo.service";
import {ItemDoacao} from "../../../models/item.doacao.model";
import {DoacaoService} from "../../../modules/doacoes/doacao.service";

@Component({
  selector: 'app-visitante-donate',
  templateUrl: './visitante-donate.component.html',
  styleUrls: ['./visitante-donate.component.scss']
})
export class VisitanteDonateComponent implements OnInit {
  model: Doacao = new Doacao();
  visitante: Visitante = new Visitante();
  abrigos: Abrigo[];
  produtos: ItemDoacao[];

  formNewDonation: FormGroup;

  blobPerfil: any;

  constructor(private visitanteService: VisitanteService,
              private localStorage: LocalStorageService,
              private formValidator: FormCustomValidator,
              private abrigoService: AbrigoService,
              private sanitizer: DomSanitizer,
              private authHttp: AuthHttp,
              private toast: ToastrService,
              private router: Router,
              private nav: NavService,
              private doacaoService: DoacaoService,
              private alertsService: AlertsService,) {
  }

  ngOnInit() {
    this.abrigos = [];
    this.produtos = null;
    this.formNewDonation = this.formValidator.createFormGroup(this.model.toFormGroup());
    this.visitanteService.getByID(this.localStorage.getVisitanteLogged()._id).then(result => {
      this.visitante.fromApi(result)
    }).catch(err => console.log(err));
    this.abrigoService.getAll().then(result => {
      this.abrigos = [];
      if (result !== null && result !== undefined && result instanceof Array) {
        for (let i = 0; i < result.length; i++) {
          let abrigo = new Abrigo();
          abrigo.fromApi(result[i]);
          this.abrigos.push(abrigo)
        }
      }
    }).catch(err => console.log(err));
    this.doacaoService.getProducts().then(result => {
      if (result !== null && result !== undefined && result instanceof Array) {
        this.produtos = [];
        for (let i = 0; i < result.length; i++) {
          let product = new ItemDoacao();
          product.fromApi(result[i]);
          this.produtos.push(product)
        }
      }
    }).catch(err => console.log(err));
  }

  confirmCancelDonationIntent() {
    this.alertsService.confirmAlert(this.cancelDonationIntent.bind(this), "Você tem certeza?", "Tem certeza que deseja cancelar esta operação?");
  }

  cancelDonationIntent() {
    this.router.navigate(["/visitante/read"])
  }

  async save() {
    if (this.formNewDonation.valid) {
      let donation = new Doacao();
      donation.doador = this.visitante;
      donation.descricao = this.formNewDonation.value.descricao;
      donation.quantidade = this.formNewDonation.value.quantidade;
      donation.status = "Aguardando";

      let prodList = this.produtos;
      for (let i = 0; i < prodList.length; i++) {
        if (prodList[i].nome == this.formNewDonation.value.item) {
          donation.item = prodList[i];
        }
      }
      let abrigoList = this.abrigos;
      for (let i = 0; i < abrigoList.length; i++) {
        if (abrigoList[i]._id == this.formNewDonation.value.abrigo) {
          donation.abrigo = abrigoList[i];
        }
      }

      this.doacaoService.create(donation).then(result => {
        swal({
          title: "Obrigado!",
          text: "A sua doação foi salva com sucesso. O abrigo entrará em contato para combinar o recolhimento do item.",
          type: 'success',
          showCancelButton: false,
          confirmButtonText: 'OK'
        }).then((result) => {
          if (result.value) {
            this.router.navigate(["/visitante/read"])
          }
        });
      }).catch(err => console.log(err));

    } else {
      this.toast.error('Formulário inválido! Corrija os campos incorretos.');
      this.formValidator.validateAllFormFields(this.formNewDonation);
    }
  }

}
