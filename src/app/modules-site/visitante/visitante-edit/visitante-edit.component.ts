import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/navigation.service';
import { Visitante } from 'src/app/models/visitante.model';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario.model';
import { Endereco } from 'src/app/models';
import { FormCustomValidator } from 'src/app/services/form-custom-validator.service';
import { UsuarioService } from 'src/app/modules/usuario/usuario.service';
import { MaskService } from 'src/app/core/mask.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { ToastrService } from 'ngx-toastr';
import { EnderecoService } from 'src/app/services/models-services/endereco.service';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';
import { MessagesCore } from 'src/app/shared/class/messagesCore.class';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AlertsService } from 'src/app/services/alerts.service';

@Component({
  selector: 'app-visitante-edit',
  templateUrl: './visitante-edit.component.html',
  styleUrls: ['./visitante-edit.component.scss']
})
export class VisitanteEditComponent implements OnInit {
  model: Visitante = new Visitante();
  usuario: Usuario = new Usuario();
  endereco: Endereco = new Endereco();

  formNewVisitante: FormGroup;
  formNewUsuarioVisitante: FormGroup;
  formNewEnderecoVisitante: FormGroup;
  
  paises = [];
  cidades = [];
  estados = [];

  mask: any = {};
  blobPerfil: any;
  constructor(
    private nav: NavService,
    private router: Router,
    private formValidator: FormCustomValidator,
    private usuarioService: UsuarioService,
    private maskService: MaskService,
    private common: CommonFunctionsService,
    private toast: ToastrService,
    private enderecoService: EnderecoService,
    private visitanteService: VisitanteService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer,
    private alert: AlertsService
  ) { 
    this.model = nav.getParameters('model');
    if (!this.model) {
      this.list();
      return;
    }

    this.usuario = this.model.usuario;
    this.endereco = this.model.endereco;
  }

  ngOnInit() {
    const services = [];
    services['usuario'] = this.usuarioService;
    console.log(this.model, this.usuario, this.endereco);
    this.formNewVisitante = this.formValidator.createFormGroup(this.model.toFormGroup());
    this.formNewUsuarioVisitante = this.formValidator.createFormGroup(this.usuario.toFormGroup(services, true));
    this.formNewEnderecoVisitante = this.formValidator.createFormGroup(this.endereco.toFormGroup());
    this.formNewUsuarioVisitante.patchValue({confpassword: this.formNewUsuarioVisitante.value.password});
    this.initMask();

    this.common.getPaises().subscribe((result) => {
      this.paises = result.filter((elem) => elem.nome_pais === 'Brasil');
    });

    this.common.getEstados().subscribe((data) => {
      // this.jsonEstadoData = data.estados;
      this.estados = data.estados;
    });
    this.carregarFoto();
  }

  carregarFoto() {
    if (this.model.fotoPerfil) {
      const loading = this.toast.info('Carregando foto....');
      this.http.getFile(this.model.fotoPerfil).then(result => {
        this.blobPerfil = result;
        this.toast.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toast.clear(loading.toastId);
      });
    }
  }


  initMask() {
    this.mask.email = this.maskService.emailMask;
    this.mask.cnpj = this.maskService.cnpjMask;
    this.mask.telefone = this.maskService.telefoneMask;
    this.mask.cep = this.maskService.cepMask;
    this.mask.data = this.maskService.dateMask;
  }

  list() {
    this.router.navigate(['/inicio']);
  }

  async save() {
    if (this.formNewUsuarioVisitante.valid && this.formNewVisitante.valid && this.formNewEnderecoVisitante.valid) {
      if (this.formNewUsuarioVisitante.get('password').value !== this.formNewUsuarioVisitante.get('confpassword').value) {
        this.toast.warning('As senhas digitadas não conferem!');
        return;
      }
      const loading = this.toast.info('Carregando');
      let backups: any = {}; // armazena os IDS, caso dê algum problema, deleta do banco
      this.model.fromFormGroup(this.formNewVisitante);
      this.endereco.fromFormGroup(this.formNewEnderecoVisitante);
      this.usuario.fromFormGroup(this.formNewUsuarioVisitante);
      // salva o usuario
      const foto = await this.saveFoto();
      this.model.fotoPerfil = foto ? foto._id : null;
      console.log(this.model, this.endereco, this.usuario);
      this.usuarioService.edit(this.usuario)
      .then((resultUsuario: any) => {
        backups['usuario'] = resultUsuario;
        // this.visitante.usuario = resultUsuario._id;
        return this.enderecoService.edit(this.endereco);
      })
      .then((resultEndereco: any) => {
        backups['endereco'] = resultEndereco;
        // this.visitante.endereco = resultEndereco._id;
        return this.visitanteService.edit(this.model);
      })
      .then((resultVisitante: any) => {
        backups['visitante'] = resultVisitante;
        this.toast.clear(loading.toastId);
        this.toast.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesEdit);
        this.router.navigate(['/visitante']);
      })
      .catch(err => {
        this.toast.clear(loading.toastId);
        console.log(`catch`, err);
        this.toast.error(MessagesCore.tituloError, err);
      });
    } else {
      this.toast.error('Formulário inválido! Corrija os campos incorretos.');
      this.formValidator.validateAllFormFields(this.formNewEnderecoVisitante);
      this.formValidator.validateAllFormFields(this.formNewVisitante);
      this.formValidator.validateAllFormFields(this.formNewUsuarioVisitante);
    }
  }

  updateCidades(estado) {
    this.cidades = estado ? estado.cidades : [];
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }

  saveFoto(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.blobPerfil) {
        resolve(await this.http.uploadFile(this.blobPerfil));
      } else {
        resolve(null);
      }
    });
  }

  uploadFile(event) {
    let file = event.target.files[0];

    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      this.blobPerfil = new Blob([reader.result], {type: 'application/octet-stream'});
      console.log(this.blobPerfil);
      // this.http.uploadFile(blob).then((data: any) => {
      //   this.source = this.sanitizer.bypassSecurityTrustUrl(data);
      // });
    }
  }

  confirmExcluir() {
    this.alert.confirmAlert(this.excluirConta.bind(this), 'Excluir a conta?', 'Você não poderá utilizá-la para acessar o sistema!');
  }

  excluirConta() {
    this.enderecoService.delete(this.endereco).then((result: any) => {
      return this.usuarioService.delete(this.usuario);
    }).then((result: any) => {
      return this.visitanteService.delete(this.model);
    }).then(result => {
      this.visitanteService.visitanteLogged = null;
      this.toast.success('Vamos sentir sua falta....', '=(');
      this.router.navigate(['/inicio']);
    }).catch(err => {
      this.toast.error(err);
    });
  }

}
