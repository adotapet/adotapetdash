import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TabsModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VisitanteReadComponent } from './visitante-read/visitante-read.component';
import { VisitanteEditComponent } from './visitante-edit/visitante-edit.component';
import { HttpModule } from '@angular/http';
import { FormUserFeedbackModule } from 'src/app/shared/modules/form-user-feedback/form-user-feedback.module';
import { TextMaskModule } from 'angular2-text-mask';
import { SolicitacaoService } from 'src/app/modules/adocoes/services/solicitacao.service';
import { PetService } from 'src/app/modules/pet/pet.service';
import {AlertsService} from "../../services/alerts.service";
import {VisitanteDonateComponent} from "./visitante-donate/visitante-donate.component";
import {DoacaoService} from "../../modules/doacoes/doacao.service";

const routes: Routes = [
  { path: '', redirectTo: 'read' },
  { path: 'read', component: VisitanteReadComponent },
  { path: 'edit', component: VisitanteEditComponent },
  {path: 'donate', component: VisitanteDonateComponent},
  // { path: 'pet', component: SearchPetComponent },
  // { path: 'pet/read', component: PetSiteReadComponent },
  // { path: 'abrigo/read', component: AbrigoSiteReadComponent }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CommonModule,
    NgSelectModule,
    HttpModule,
    FormUserFeedbackModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    TextMaskModule
  ],
  providers: [
    AlertsService,
    PetService,
    SolicitacaoService,
    DoacaoService,
  ],
  declarations: [VisitanteReadComponent, VisitanteEditComponent, VisitanteDonateComponent]
})
export class VisitanteModule { }
