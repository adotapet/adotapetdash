import {Component, Inject, OnInit} from '@angular/core';
import { Visitante } from 'src/app/models/visitante.model';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NavService } from 'src/app/services/navigation.service';
import {AlertsService} from "../../../services/alerts.service";
import {SESSION_STORAGE, StorageService} from "angular-webstorage-service";
import swal from "sweetalert2";
import { SolicitacaoService } from 'src/app/modules/adocoes/services/solicitacao.service';
import { PetService } from 'src/app/modules/pet/pet.service';
import {Doacao} from "../../../models/doacao.model";
import {DoacaoService} from "../../../modules/doacoes/doacao.service";
import {ItemDoacao} from "../../../models/item.doacao.model";

@Component({
  selector: 'app-visitante-read',
  templateUrl: './visitante-read.component.html',
  styleUrls: ['./visitante-read.component.scss']
})
export class VisitanteReadComponent implements OnInit {
  model: Visitante = new Visitante();
  solicitacoes: any[] = [];
  doacoes: Doacao[] = [];

  blobPerfil: any;
  constructor(
    private visitanteService: VisitanteService,
    private localStorage: LocalStorageService,
    private sanitizer: DomSanitizer,
    private authHttp: AuthHttp,
    private toast: ToastrService,
    private router: Router,
    private nav: NavService,
    private petService: PetService,
    private alertsService: AlertsService,
    private solicitacaoService: SolicitacaoService,
    private doacaoService: DoacaoService,
  ) { }

  ngOnInit() {
    this.solicitacaoService.getSolicitacoesByVisitanteID(this.localStorage.getVisitanteLogged()._id).then((result: any) => {
      console.log('solicitacoes', result);
      result.forEach(async (sol) => {
        if (sol.pet.fotoPerfil) {
          sol.pet.fotoPerfil = await this.authHttp.getFile(sol.pet.fotoPerfil ? sol.pet.fotoPerfil._id || sol.pet.fotoPerfil : '');
        }
      });
      this.solicitacoes = result.reverse();
    });
    this.visitanteService.getByID(this.localStorage.getVisitanteLogged()._id).then((result: any) =>{
      this.localStorage.setVisitanteLogged(result);
      this.model.fromApi(result);
      this.carregarFoto();
      this.listDonations();
    }).catch(err => console.log(err));
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) :
    '../../../../assets/dog-icon.png';
  }

  getSolicitacaoFoto(foto) {
    return foto && typeof foto !== 'string' ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(foto)) :
    '../../../../assets/dog-icon.png';
  }

  carregarFoto() {
    if (this.model.fotoPerfil) {
      const loading = this.toast.info('Carregando foto....');
      this.authHttp.getFile(this.model.fotoPerfil ? this.model.fotoPerfil._id || this.model.fotoPerfil : null).then(result => {
        this.blobPerfil = result;
        this.toast.clear(loading.toastId);
      }).catch(err => {
        console.log(err);
        this.toast.clear(loading.toastId);
      });
    }
  }

  edit() {
    this.nav.clearParameters();
    this.nav.setParameters('model', this.model);
    this.router.navigate(['/visitante/edit']);
  }

  createDonation() {
    this.nav.clearParameters();
    this.nav.setParameters('model', this.model);
    this.router.navigate(["/visitante/donate"]);
  }

  confirmCancelDonation(donation) {
    let subtitle = "Tem certeza que deseja cancelar a doação de " + donation.quantidade
      + " unidade(s) de " + donation.item.nome
      + " para o abrigo " + donation.abrigo.nome + "?";
    this.alertsService.confirmAlert(this.cancelDonation.bind(this, donation), "Você tem certeza?", subtitle);
  }

  cancelDonation(donation) {
    this.doacaoService.cancel(donation).then(result => {
      this.listDonations();
      swal({
        title: "Doação cancelada com sucesso",
        text: "Sabemos que imprevistos que venham a impossibilitar uma doação podem acontecer.\nEsperamos que um dia possa realizar esta doação, nossos animaizinhos vão adorar :D",
        type: 'success',
        showCancelButton: false,
        confirmButtonText: 'OK'
      }).then((result) => {
      });
    }).catch(err => {
      console.log(err);
    });
  }

  listDonations() {
    this.doacaoService.getByUserID(this.model._id).then(result => {
      this.doacoes = [];
      if (result !== null && result !== undefined && result instanceof Array) {
        for (let i = 0; i < result.length; i++) {
          let donation = new Doacao();
          donation.fromApi(result[i]);
          this.doacoes.push(donation);
        }
      }
    }).catch(err => {
      console.log(err);
    });
  }


}
