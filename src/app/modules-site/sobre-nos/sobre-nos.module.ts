import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SobreNosComponent } from './sobre-nos.component';
import { Routes, RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { TabsModule, ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CommonFunctionsService } from '../../services/common-functions.service';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';

const routes: Routes = [
  {
    path: '',
    component: SobreNosComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    CommonModule,
    FormUserFeedbackModule
  ],
  declarations: [SobreNosComponent],
  exports: [RouterModule],
  providers: [
    CommonFunctionsService
  ]
})
export class SobreNosModule { }
