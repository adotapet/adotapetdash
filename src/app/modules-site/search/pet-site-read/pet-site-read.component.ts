import { Component, OnInit, ViewChild } from '@angular/core';
import { Pet } from '../../../models/pet.model';
import { Router } from '@angular/router';

import { PetService } from '../../../modules/pet/pet.service';
import { AuthHttp } from '../../../services/auth.http.service';
import { ToastrService } from '../../../../../node_modules/ngx-toastr';
import { DomSanitizer } from '../../../../../node_modules/@angular/platform-browser';
import { BsModalService } from 'ngx-bootstrap';
import { Solicitacao } from 'src/app/models/solicitacao.model';
import { FormGroup } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { FormCustomValidator } from 'src/app/services/form-custom-validator.service';
import { SolicitacaoService } from 'src/app/modules/adocoes/services/solicitacao.service';
import { Visitante } from 'src/app/models/visitante.model';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';
import { MaskService } from 'src/app/core/mask.service';

@Component({
  selector: 'app-pet-site-read',
  templateUrl: './pet-site-read.component.html',
  styleUrls: ['./pet-site-read.component.scss']
})
export class PetSiteReadComponent implements OnInit {
  model: Pet = new Pet();
  solicitacao: Solicitacao = new Solicitacao();
  form: FormGroup;
  modalAdocao: any;
  blobPerfil: any;
  mask: any = {};
  solicitacoesVisitante: any;

  opcoesRendaMensal = [];

  constructor(private petService: PetService,
    private router: Router,
    private http: AuthHttp,
    private toast: ToastrService,
    private modalService: BsModalService,
    private localStorage: LocalStorageService,
    private formCustom: FormCustomValidator,
    private solicitacaoService: SolicitacaoService,
    private visitanteService: VisitanteService,
    private maskService: MaskService,
    private sanitizer: DomSanitizer) {
    this.model = petService.readingPet;

    this.solicitacaoService.getFaixasSalariais().then((res: any) => {
      this.opcoesRendaMensal = res.object.questionario.faixaSalarial.enum;
      console.log(this.opcoesRendaMensal);
    }).catch((err) => {
      console.log(err);
    });

    if (!petService.readingPet) {
      console.log('BACK');
      this.voltar();
    }
  }

  ngOnInit() {
    this.form = this.solicitacao.toFormGroup();
    this.initMask();

    if (this.localStorage.getVisitanteLogged()) {
      this.solicitacaoService.getSolicitacoesByVisitanteID(this.localStorage.getVisitanteLogged()._id).then(result => {
        this.solicitacoesVisitante = result ? result : [];
      });
    }
    console.log(this.model);
  }

  initMask() {
    this.mask.money = this.maskService.moneyMask;
  }

  adotar() {
    this.form.patchValue({
      adotante: this.localStorage.getVisitanteLogged()._id,
      pet: this.model._id
    });
    if (this.form.valid) {
      this.solicitacao.fromFormGroup(this.form);
      const loading = this.toast.info('Carregando...');
      this.solicitacaoService.create(this.solicitacao).then((result: any) => {
        let visitanteLogged = this.localStorage.getVisitanteLogged();
        visitanteLogged.solicitacoes.push(result);
        this.solicitacoesVisitante.push(result);
        this.localStorage.setVisitanteLogged(visitanteLogged);
        visitanteLogged.solicitacoes = visitanteLogged.solicitacoes.map(sol => sol._id);
        return this.visitanteService.edit(visitanteLogged);
      }).then((result: any) => {
        this.toast.clear(loading.toastId);
        this.toast.success('Nós analisaremos o questionário e entraremos em contato!', 'Solicitação efetuada com sucesso!');
        this.modalAdocao.hide();
      }).catch(err => {
        this.toast.clear(loading.toastId);
        console.log('erro' , err);
        this.toast.error(err);
      });
    } else {
      this.toast.error('Preencha os campos corretamente!');
      this.formCustom.validateAllFormFields(this.form);
      console.log('form invlaido', this.form);
    }
  }

  podeAdotar() {
    if (this.model.adocao.status !== 'Disponível para Adoação') {
      return false;
    } else if (this.solicitacoesVisitante) {
      let jaSolicitado = this.solicitacoesVisitante.find(sol => {
        if (sol.pet._id) {
          return sol.pet._id === this.model._id;
        } else {
          return sol.pet === this.model._id;
        }
      });
      return jaSolicitado ? false : true;
    } else {
      return false;
    }
  }

  getError() {
    if (this.model.adocao.status === 'Adotado') {
      return 'Este pet já foi adotado! =D';
    } else if (this.model.adocao.status !== 'Disponível para Adoação') {
      return 'Este Pet não está disponível para adoção no momento!';
    } else if (this.localStorage.getVisitanteLogged()) {
      return 'Você já realizou uma solicitação para este PET.';
    } else {
      return 'Você precisa estar logado para realizar uma solicitação!';
    }
  }

  getFoto() {
    return this.model.fotoPerfil ? this.model.fotoPerfil : '../../../../assets/dog-icon.png';
  }

  voltar() {
    this.router.navigate(['/search/pet']);

  }

  openModal(modal) {
    this.modalAdocao = this.modalService.show(modal);
  }

}
