import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetSiteReadComponent } from './pet-site-read.component';

describe('PetSiteReadComponent', () => {
  let component: PetSiteReadComponent;
  let fixture: ComponentFixture<PetSiteReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetSiteReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetSiteReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
