import { Component, OnInit } from '@angular/core';
import { Abrigo } from '../../../models';
import { AbrigoService } from '../../../modules/abrigo/abrigo.service';
import { Router } from '@angular/router';
import { PetService } from 'src/app/modules/pet/pet.service';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-abrigo-site-read',
  templateUrl: './abrigo-site-read.component.html',
  styleUrls: ['./abrigo-site-read.component.scss']
})
export class AbrigoSiteReadComponent implements OnInit {
  abrigo: Abrigo = new Abrigo();
  pets: any[] = [];
  constructor(
    public abrigoService: AbrigoService,
    public router: Router,
    public petsService: PetService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer,
    private petService: PetService) {
    if (!this.abrigoService.abrigo) {
      this.router.navigate(['/search/abrigo']);
    }
    this.abrigo = this.abrigoService.abrigo;
  }

  async ngOnInit() {
    this.pets = await this.petsService.getPetsFromAbrigo(this.abrigoService.abrigo._id);
    this.pets.forEach(pet => {
      this.carregarFoto(pet);
    });
  }

  carregarFoto(pet) {
    if (pet.fotoPerfil) {
      this.http.getFile(pet.fotoPerfil ? pet.fotoPerfil._id || pet.fotoPerfil : null).then(result => {
        pet.fotoPerfil = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(result));
      }).catch(err => {
        console.log(err);
      });
    } else {
      pet.fotoPerfil = '../../../../assets/dog-icon.png';
    }
  }

  verDetalhesPet(pet) {
    this.petService.readingPet = pet;
    this.router.navigate(['/search/pet/read']);
  }

}
