import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PetService } from '../../../modules/pet/pet.service';
import { AuthHttp } from '../../../services/auth.http.service';
import { DomSanitizer } from '../../../../../node_modules/@angular/platform-browser';

@Component({
  selector: 'app-search-pet',
  templateUrl: './search-pet.component.html',
  styleUrls: ['./search-pet.component.scss']
})
export class SearchPetComponent implements OnInit {
  private pets = [];

  nomeInput: string = '';
  especieInput: string = '';
  racaInput: string = '';
  porteInput: string = 'Médio';

  filtro : any = {};
  constructor(
    private router: Router,
    private petService: PetService,
    private http: AuthHttp,
    private sanitizer: DomSanitizer
  ) {

  }

  ngOnInit() {
    this.filtro = this.petService.getSearch();
    this.nomeInput = this.filtro['nome'] ? this.filtro['nome'] : '';
    this.racaInput = this.filtro['raca'] ? this.filtro['raca'] : '';
    this.porteInput = this.filtro['porte'] ? this.filtro['porte'] : 'Todos';
    this.search();
  }

  carregarFoto(pet) {
    if (pet.fotoPerfil) {
      this.http.getFile(pet.fotoPerfil ? pet.fotoPerfil._id || pet.fotoPerfil : null).then(result => {
        pet.fotoPerfil = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(result));
      }).catch(err => {
        console.log(err);
      });
    } else {
      pet.fotoPerfil = '../../../../assets/dog-icon.png';
    }
  }

  verDetalhesPet(pet) {
    this.petService.readingPet = pet;
    this.router.navigate(['/search/pet/read']);

  }

  applyFilter() {
    this.nomeInput ? this.filtro.nome = this.nomeInput : delete this.filtro.nome;
    this.racaInput ? this.filtro.raca = this.racaInput : delete this.filtro.raca;
    this.especieInput ? this.filtro.especie = this.especieInput : delete this.filtro.especie;
    this.porteInput !== 'Todos' ? this.filtro.porte = this.porteInput : delete this.filtro.porte;
    this.search();
  }

  search() {
    this.petService.search(this.filtro).then((res: any[]) => {
      this.pets = res.reverse();
      this.pets.forEach(pet => {
      this.carregarFoto(pet);
      });
    }).catch((err) => {
      console.log(err);
    });
  }

  limparFiltros() {
    this.nomeInput = '';
    this.racaInput = '';
    this.especieInput = '';
    this.porteInput = 'Todos';
    this.applyFilter();
  }

}
