import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchAbrigoComponent } from './search-abrigo/search-abrigo.component';
import { Routes, RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonFunctionsService } from '../../services/common-functions.service';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AbrigoSiteReadComponent } from './abrigo-site-read/abrigo-site-read.component';
import { TabsModule } from 'ngx-bootstrap';
import { SearchPetComponent } from './search-pet/search-pet.component';
import { PetSiteReadComponent } from './pet-site-read/pet-site-read.component';
import { SobreNosComponent } from '../sobre-nos/sobre-nos.component';
import { FormUserFeedbackModule } from 'src/app/shared/modules/form-user-feedback/form-user-feedback.module';
import { FormCustomValidator } from 'src/app/services/form-custom-validator.service';
import { SolicitacaoService } from 'src/app/modules/adocoes/services/solicitacao.service';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';
import { MaskService } from 'src/app/core/mask.service';
import { TextMaskModule } from 'angular2-text-mask';

// import { PetService } from '../../modules/pet/pet.service';

const routes: Routes = [
  { path: 'abrigo', component: SearchAbrigoComponent },
  { path: 'pet', component: SearchPetComponent },
  { path: 'pet/read', component: PetSiteReadComponent },
  { path: 'abrigo/read', component: AbrigoSiteReadComponent }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    TabsModule.forRoot(),
    TextMaskModule,
    HttpClientModule,
    CommonModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    FormUserFeedbackModule
  ],
  declarations: [SearchAbrigoComponent, AbrigoSiteReadComponent, SearchPetComponent, PetSiteReadComponent],
  providers: [
    CommonFunctionsService,
    FormCustomValidator,
    SolicitacaoService,
    VisitanteService,
    MaskService
  ]
})
export class SearchModule { }
