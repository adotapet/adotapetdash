import { Component, OnInit } from '@angular/core';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { Router } from '@angular/router';
import { AbrigoService } from '../../../modules/abrigo/abrigo.service';


@Component({
  selector: 'app-search-abrigo',
  templateUrl: './search-abrigo.component.html',
  styleUrls: ['./search-abrigo.component.scss']
})
export class SearchAbrigoComponent implements OnInit {

  abrigosFound = [];

  estados: any[];
  cidades: any[];

  abrigoNomeInput: string = '';
  cidadeSelected: any;
  estadoSelected: any;

  filtro : any = {};
  constructor(
    private commom: CommonFunctionsService,
    private router: Router,
    private abrigoService: AbrigoService) {

    }

  ngOnInit() {
    this.loadEstados();
    let prefilter = this.abrigoService.getSearch();
    prefilter['nome'] ? this.abrigoNomeInput = prefilter['nome'] : '';
    prefilter['estado'] ? this.estadoSelected = prefilter['estado'] : null;
    prefilter['cidade'] ? this.cidadeSelected = prefilter['cidade'] : null;
    this.applyFilter();
  }

  loadEstados() {
    this.commom.getEstados().subscribe(result => {
      this.estados = result.estados;
    });
  }

  updateCidades(estado) {
    this.cidadeSelected = null;
    this.cidades = estado ? estado.cidades : [];
  }

  read(abrigo) {
    this.abrigoService.abrigo = abrigo;
    this.router.navigate(['/search/abrigo/read']);
  }

  applyFilter() {
    this.abrigoNomeInput ? this.filtro.nome = this.abrigoNomeInput : delete this.filtro.nome;
    this.estadoSelected ? this.filtro['endereco.uf'] = this.estadoSelected : delete this.filtro['endereco.uf'];
    this.cidadeSelected ? this.filtro['endereco.cidade'] = this.cidadeSelected : delete this.filtro['endereco.cidade'];
    this.search();
  }

  search() {
    this.abrigoService.search(this.filtro).then((res: any[]) => {
      console.log(res);
      this.abrigosFound = res;
    }).catch((err) => {
      console.log(err);
    });
  }

  limparFiltros() {
    this.abrigoNomeInput = '';
    this.estadoSelected = null;
    this.cidadeSelected = null;
    this.applyFilter();
  }

}
