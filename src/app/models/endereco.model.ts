import { Validators, AbstractControl } from "@angular/forms";

export class Endereco {
  _id: string;
  logradouro: string;
  numero: string;
  pais: string;
  uf: string;
  cep: string;
  cidade: string;
  bairro: string;
  complemento: string;

  constructor () {

  }

  fromApi(json) {
    Object.assign(this, json);
  }

  toFormGroup(required = true) {
    const pais = this.pais ? this.pais : 'Brasil';
    return {
      logradouro: [this.logradouro, required ? Validators.required : []],
      numero: [this.numero, required ? Validators.required : []],
      pais: [pais, required ? Validators.required : []],
      uf: [this.uf, required ? Validators.required : []],
      cep: [this.cep, required ? [Validators.required, Validators.pattern('[0-9]{2}\\.[0-9]{3}-[0-9]{3}')] : [Validators.pattern('[0-9]{2}\\.[0-9]{3}-[0-9]{3}')]],
      cidade: [this.cidade, required ? Validators.required : []],
      bairro: [this.bairro, required ? Validators.required : []],
      complemento: [this.complemento],
    }
  }

  fromFormGroup(form: AbstractControl) {
    this.logradouro = form.value.logradouro; 
    this.numero = form.value.numero; 
    this.pais = form.value.pais; 
    this.uf = form.value.uf;
    this.cep = form.value.cep;
    this.cidade = form.value.cidade; 
    this.bairro = form.value.bairro; 
    this.complemento = form.value.complemento; 
  }
}
