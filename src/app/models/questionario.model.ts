import { Endereco } from './endereco.model';
import { Responsavel } from './responsavel.model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidatorsExtra } from '../shared/class/validatorsExtra.class';

export class Questionario {
  _id: string;  

  constructor () {
   
  }

  toFormGroup(services: any, isEdit = false) {
    const formGroup = new FormBuilder();
    return {
      // funcionarios: [this.funcionarios],
      // pets: [this.pets],
      // endereco: [this.endereco],
      // nome: [this.nome, Validators.required, new ValidatorsExtra().isUnique(services['abrigo'], 'nome', isEdit)],
      // cnpj: [this.cnpj, [Validators.required, Validators.pattern('[0-9]{2}\.[0-9]{3}\.[0-9]{3}\/[0-9]{4}-[0-9]{2}')], new ValidatorsExtra().isUnique(services['abrigo'], 'cnpj', isEdit)],
      // email: [this.email, [Validators.required, Validators.email], new ValidatorsExtra().isUnique(services['abrigo'], 'email', isEdit)],
      // telefone: [this.telefone, [], new ValidatorsExtra().isUnique(services['abrigo'], 'telefone', isEdit)],
      // descricao: [this.descricao],
      // responsavel: formGroup.group(this.responsavel.toFormGroup()),
    }    
  }

  fromFormGroup(form: FormGroup) {
    // this.funcionarios = form.value.funcionarios;
    // this.pets = form.value.pets;
    // this.endereco = form.value.endereco;
    // this.nome = form.value.nome;
    // this.cnpj = form.value.cnpj;
    // this.email = form.value.email;
    // this.telefone = form.value.telefone;
    // this.descricao = form.value.descricao;
    // this.responsavel.fromFormGroup(form.get('responsavel'));
  }

  fromApi(data) {
    // Object.assign(this, data);
    // this.endereco = Object.assign(new Endereco(), data.endereco);
    // this.responsavel = Object.assign(new Responsavel(), data.responsavel);
  }
}
