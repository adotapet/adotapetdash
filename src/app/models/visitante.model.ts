import { Endereco } from './endereco.model';
import { Responsavel } from './responsavel.model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidatorsExtra } from '../shared/class/validatorsExtra.class';
import { Usuario } from './usuario.model';

export class Visitante {
  _id: string;
  nome: string;
  dataNascimento: any;
  fotoPerfil: any;
  usuario: any;
  solicitacoes: any[];
  endereco: any;

  constructor () {
    this.solicitacoes = [];
    this.usuario = new Usuario();
    this.endereco = new Endereco();
  }

  toFormGroup() {
    const formGroup = new FormBuilder();
    return {
      nome: [this.nome, [Validators.required]],
      dataNascimento: [this.dataNascimento, [Validators.required, ValidatorsExtra.isDate]],
    }    
  }

  fromFormGroup(form: FormGroup) {
    this.nome = form.value.nome;
    this.dataNascimento = ValidatorsExtra.convertStringToDate(form.value.dataNascimento);
    // this.usuario = form.value.usuario;
    // this.endereco = form.value.endereco;
  }

  fromApi(data) {
    Object.assign(this, data);
    this.endereco = Object.assign(new Endereco(), data.endereco);
    this.usuario = Object.assign(new Usuario(), data.usuario);
  }
}
