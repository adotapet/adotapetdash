import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {Visitante} from "./visitante.model";
import {Abrigo} from "./abrigo.model";


export class ItemDoacao {

  nome: String;
  categoria: String;

  constructor() {

  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return {
      nome: [this.nome, [Validators.required]],
      categoria: [this.categoria, [Validators.required]],
    }
  }

  fromFormGroup(form: FormGroup) {
    this.nome = form.value.nome;
    this.categoria = form.value.categoria;
  }

  fromApi(data) {
    Object.assign(this, data);
  }

}
