import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { ValidatorsExtra } from "../shared/class/validatorsExtra.class";

export class Vacina {
	_id: string;
	nome: string;
	data: any;
	descricao: string;
	aplicada: boolean;
	constructor() {
		
	};

	fromApi(json) {
		Object.assign(this, json);
	}

	toApi() {
		let json = Object.assign({}, this);
		delete json['_id'];
		return json;
	}

	toFormGroup() {
		return {
			_id: [this._id],
			nome: [this.nome, [Validators.required]],
			data: [this.data, [ValidatorsExtra.isDate]],
			descricao: [this.descricao, [Validators.required]],
			aplicada: [this.aplicada ? this.aplicada : false, [Validators.required]],
		}
	}

	fromFormGroup(form: FormGroup) {
		this.cleanModel();
		Object.assign(this, form.value);
    this.data = ValidatorsExtra.convertStringToDate(form.value.data);
	}

	cleanModel() {
		Object.getOwnPropertyNames(this).forEach((prop) => {
			if (prop !== '_id' && prop !== '_v') {
				this[prop] = null;
			}
		});
		Object.assign(this, new Vacina());
	}

	popularVacinacao(vacinas: Vacina[]) {
		let forms = [];
		const formBuilder = new FormBuilder();
		vacinas.forEach((el) => {
			let vacina: any = new Vacina();
			vacina.fromApi(el);
			forms.push(formBuilder.group(vacina.toFormGroup()));
		});
		return forms;
	}
}