import { Validators, FormBuilder } from "@angular/forms";

export class Medicamento {
	_id: string;
	usoContinuo: boolean;
	nome: string;
	apresentacao: string;
	dosagem: string;
	
	
	constructor() {
		
	}

	toFormGroup() {
		return {
			_id: [this._id],
			usoContinuo: [this.usoContinuo ? this.usoContinuo : false, [Validators.required]],
			nome: [this.nome, [Validators.required]],
			apresentacao: [this.apresentacao, [Validators.required]],
			dosagem: [this.dosagem, [Validators.required]],
		};
	}

	fromApi(json) {
		Object.assign(this, json);
	}

	popularMedicamento(medicamentos: Medicamento[]) {
		let forms = [];
		const formBuilder = new FormBuilder();
		medicamentos.forEach((el) => {
			let medicamento: any = new Medicamento();
			medicamento.fromApi(el);
			forms.push(formBuilder.group(medicamento.toFormGroup()));
		});
		return forms;
	}

}