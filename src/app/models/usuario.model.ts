import { Endereco } from './endereco.model';
import { Responsavel } from './responsavel.model';
import { Validators, FormGroup } from '@angular/forms';
import { ValidatorsExtra } from '../shared/class/validatorsExtra.class';
import { UsuarioService } from '../modules/usuario/usuario.service';
import { AuthHttp } from '../services/auth.http.service';

export class Usuario {
  _id: any;
  // nome: string;
  email: string;
  username: string;
  password: string;

  constructor () {
  }

  fromApi(json) {
    Object.assign(this, json);
  }

  toFormGroup(services: any[], isEdit = false) {
    return {
      // nome: [this.nome, [Validators.required]],
      email: [this.email, [Validators.required, Validators.email], new ValidatorsExtra().isUnique(services['usuario'], 'email', isEdit)],
      username: [this.username, Validators.required, new ValidatorsExtra().isUnique(services['usuario'], 'username', isEdit)],
      password: [this.password, [Validators.required, ValidatorsExtra.validarSenha]],
      confpassword: ['', Validators.required],
    }
  }

  fromFormGroup(form: FormGroup) {
    // this.nome = form.value.nome;
    this.email = form.value.email;
    this.username = form.value.username;
    this.password = form.value.password;
  }
}
