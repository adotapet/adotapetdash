import { Validators, FormBuilder, FormGroup } from '@angular/forms';


export class Adocao {

  static BOOLEAN_VALUES = [
    {id: true, label: 'Sim'},
    {id: false, label: 'Não'}
  ];

  codigo: number;
  status: number; // 1 - Pedido de Adoção, 2 - Avaliando , 3 - Aceito , 4 - Adotado , 5 - Rejeitado

  constructor () {

  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return {

    }
  }

  fromFormGroup(form: FormGroup) {
    Object.assign(this, form.value);
  }

  fromApi(data) {
    Object.assign(this, data);
  }

  getStatus() {
    return [
      { id: 1, label: 'Solicitado' },
      { id: 2, label: 'Em Avaliação' },
      { id: 3, label: 'Aceito' },
      { id: 4, label: 'Adotado' },
      { id: 5, label: 'Rejeitado' }
    ]
  }
}
