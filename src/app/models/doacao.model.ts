import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {Visitante} from "./visitante.model";
import {Abrigo} from "./abrigo.model";
import {ItemDoacao} from "./item.doacao.model";


export class Doacao {

  _id: String;
  abrigo: Abrigo;
  doador: Visitante;
  status: String;
  quantidade: number;
  item: ItemDoacao;
  descricao: String;

  constructor () {
    this.item = new ItemDoacao();
    this.doador = new Visitante();
  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return {
      abrigo: [this.abrigo, [Validators.required]],
      quantidade: [this.quantidade, [Validators.min(1), Validators.required]],
      item: [this.item, [Validators.required]],
      descricao: [this.descricao, [Validators.required]],
    }
  }

  fromFormGroup(form: FormGroup) {
    this.abrigo = form.value.abrigo;
    this.quantidade = form.value.quantidade;
    this.descricao = form.value.descricao;
  }

  fromApi(data) {
    Object.assign(this, data);
    this.abrigo = new Abrigo();
    this.abrigo.fromApi(data.abrigo);
    this.doador = data.doador;
    this.item = new ItemDoacao();
    this.item.fromApi(data.item);
  }

}
