import { Endereco } from './endereco.model';
import { Responsavel } from './responsavel.model';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Medicamento } from './medicamento.model';
import { Vacina } from './vacina.model';
import { ValidatorsExtra } from '../shared/class/validatorsExtra.class';

export class Alimentacao {
  nome: string;
  constructor() {
  }

  fromApi(json) {
    this.nome = json;
  }

  toFormGroup() {
    return {
      nome: [this.nome, [Validators.required]]
    }
  }

  popularAlimentacao(alimentacao: Alimentacao[]) {
		let forms = [];
		const formBuilder = new FormBuilder();
		alimentacao.forEach((el) => {
			let alimento: any = new Alimentacao();
			alimento.fromApi(el);
			forms.push(formBuilder.group(alimento.toFormGroup()));
		});
		return forms;
	}
}

export class Pet {

  static BOOLEAN_VALUES = [
    {id: true, label: 'Sim'},
    {id: false, label: 'Não'}
  ];

  static SEXO = [
    {id: true, label: 'Masculino'},
    {id: false, label: 'Feminino'}
  ];

  static PORTE = [
    'Mini', 'Pequeno', 'Médio', 'Grande'
  ];

  _id: string;
  nome: string;
  dataNascimento: any;
  especie: string;
  raca: string;
  pelagem: string;
  peso: string;
  porte: string;
  adocao: any;
  historia: string;
  sexo: boolean;
  castrado: boolean;
  medicamentosEspecificos: Medicamento[];
  alimentacoesEspecificas: any[];
  deficienciasOuDoencas: string;
  vacinacoes: Vacina[];
  fotoPerfil: any;
  caracteristicas: any;
  abrigo: any;
  // medicamentosEspecificos: boolean;
  // alimentacoesEspecificas: boolean;

  constructor () {
    this.medicamentosEspecificos = [];
    this.alimentacoesEspecificas = [];
    this.vacinacoes = [];
    // this.caracteristicas = {};
  }

  toApi(abrigoid?:string) {
    let json = Object.assign({}, this);
    json.abrigo = abrigoid;
    json.alimentacoesEspecificas = json.alimentacoesEspecificas.map(el => {return el.nome});
    console.log(json);
    return json;
  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return {
      nome: [this.nome, [Validators.required]],
      dataNascimento: [this.dataNascimento, [Validators.required, ValidatorsExtra.isDate]],
      especie: [this.especie, [Validators.required]],
      raca: [this.raca, [Validators.required]],
      pelagem: [this.pelagem, [Validators.required]],
      peso: [this.peso, [Validators.required]],
      porte: [this.porte, [Validators.required]],
      historia: [this.historia],
      sexo: [this.sexo ? this.sexo : false, [Validators.required]],
      castrado: [this.castrado ? this.castrado : false, [Validators.required]],
      medicamentosEspecificos: formBuilder.array(this.medicamentosEspecificos ? new Medicamento().popularMedicamento(this.medicamentosEspecificos) : []),
      alimentacoesEspecificas: formBuilder.array(this.alimentacoesEspecificas ? new Alimentacao().popularAlimentacao(this.alimentacoesEspecificas) : []),
      deficienciasOuDoencas: [this.deficienciasOuDoencas],
      vacinacoes: formBuilder.array(this.vacinacoes ? new Vacina().popularVacinacao(this.vacinacoes) : []),
      caracteristicas: [this.caracteristicas],
      abrigo: [this.abrigo]
      // medicamentosEspecificos: [this.medicamentosEspecificos ? this.medicamentosEspecificos : false, [Validators.required]],
      // alimentacoesEspecificas: [this.alimentacoesEspecificas ? this.alimentacoesEspecificas : false, [Validators.required]],
    }
  }

  fromFormGroup(form: FormGroup) {
    this.cleanModel();
    let obj: any = Object.assign(this, form.value);
    this.dataNascimento = ValidatorsExtra.convertStringToDate(form.value.dataNascimento);
    Object.keys(obj).forEach(ix => {
      if (obj[ix] === null || obj[ix] === undefined) {
        delete obj[ix];
      }
    });

  }

  fromApi(data) {
    Object.assign(this, data);
    // this.vacinacoes = this.vacinacoes.map(el => Object.assign(new Vacina(), el));
    // this.medicamentosEspecificos = this.medicamentosEspecificos.map(el => Object.assign(new Medicamento(), el));
    console.log(this);
  }

  cleanModel() {
		Object.getOwnPropertyNames(this).forEach((prop) => {
			if (prop !== '_id' && prop !== '_v') {
				this[prop] = null;
			}
		});
		Object.assign(this, new Pet());
	}
}
