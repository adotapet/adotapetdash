import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Endereco } from './endereco.model';


export class Funcionario {
  _id: string;
  fotoPerfil: string;
  abrigo: string;
  usuario: any;
  permissao: any;
  nome: string;
  endereco: string;
  telefone: string;

  constructor () {

  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return {
      fotoPerfil: [this.fotoPerfil, []],
      abrigo: [this.abrigo, []],
      usuario: [this.usuario, []],
      permissao: [this.permissao, []],
      nome: [this.nome, [Validators.required]],
      endereco: [this.endereco, []],
      telefone: [this.telefone, [Validators.required]],
    }
  }

  fromFormGroup(form: FormGroup) {
    Object.assign(this, form.value);
  }

  fromApi(data) {
    Object.assign(this, data);
  }

}
