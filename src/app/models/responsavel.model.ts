import { Validators, FormGroup, AbstractControl } from "@angular/forms";

export class Responsavel {

  nome: string;
  email: string;
  telefone: string;

  constructor () {

  }

  toFormGroup() {
    return {
      nome: [this.nome, Validators.required],
      email: [this.email, [Validators.required, Validators.email]],
      telefone: [this.telefone, [Validators.required]],  
    }
  }

  fromFormGroup(form: AbstractControl) {
    this.nome = form.value.nome;
    this.email = form.value.email;
    this.telefone = form.value.telefone;
  }
}
