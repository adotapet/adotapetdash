import { Validators, FormBuilder, FormGroup } from '@angular/forms';


export class Solicitacao {

  static FAIXA_SALARIAL = [
    'Menos que R$400,00',
    'R$ 400,00 à 600,00',
    'R$ 600,00 à 1.200,00',
    'R$ 1.200,00 à 1.600,00',
    'R$ 1.600,00 à 2.600,00',
    'Mais de R$ 2.600,00'
  ];

  static BOOLEAN_VALUES = [
    {id: true, label: 'Sim'},
    {id: false, label: 'Não'}
  ];
  
  _id: string;
  codigo: number;
  status: number; // 1 - Pedido de Adoção, 2 - Avaliando , 3 - Aceito , 4 - Adotado , 5 - Rejeitado
  data: any;
  motivo: string;
  questionario: any;
  adotante: any;
  pet: any;

  constructor () {
    this.questionario = {};
  }

  toFormGroup() {
    const formBuilder = new FormBuilder();
    return formBuilder.group({
      adotante: [this.adotante, Validators.required],
      pet: [this.pet, Validators.required],
      data: [this.data],
      status: [this.status],
      motivo: [this.motivo],
      questionario: formBuilder.group({
        faixaSalarial: [this.questionario.faixaSalarial, Validators.required],
        teveAnimais: [this.questionario.teveAnimais, Validators.required],
        temAnimais: [this.questionario.temAnimais, Validators.required],
        temCriancas: [this.questionario.temCriancas, Validators.required],
        viajaFrequentemente: [this.questionario.viajaFrequentemente, Validators.required],
        resideEm: [this.questionario.resideEm, Validators.required],
        redeProtecao: [this.questionario.redeProtecao, Validators.required],
        razao: [this.questionario.razao, Validators.required],
        rotina: [this.questionario.rotina, Validators.required],
      })
    })
  }

  fromFormGroup(form: FormGroup) {
    let obj: any = Object.assign(this, form.value);
    Object.keys(obj).forEach(ix => {
      if (obj[ix] === null || obj[ix] === undefined) {
        delete obj[ix];
      }
    });
  }

  fromApi(data) {
    Object.assign(this, data);
  }

  getStatus() {
    return [
      { id: 1, label: 'Solicitado' },
      { id: 2, label: 'Em Avaliação' },
      { id: 3, label: 'Aceito' },
      { id: 4, label: 'Adotado' },
      { id: 5, label: 'Rejeitado' }
    ]
  }
}
