import { Injectable } from '@angular/core';
import { FormControl, FormGroup, AbstractControl, FormBuilder, FormArray } from '@angular/forms';

@Injectable()
export class FormCustomValidator {
	private alertsMsg: any;

	constructor(private formBuilder: FormBuilder) {

	}

	public validateAllFormFields(formGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field);
			if (control instanceof FormControl && control.enabled) {
				control.markAsTouched({onlySelf: true});
			} else if (control instanceof FormGroup || control instanceof FormArray) {
				this.validateAllFormFields(control);
			}
		});
	}

	public isFieldValid(field: string, form: FormGroup) {
		return !form.get(field).valid && form.get(field).touched;
	}

	public displayFieldCss(field: string, form: FormGroup) {
		return{
			'has-error': this.isFieldValid(field, form),
			'has-valid': !this.isFieldValid(field, form) && form.get(field).touched
		};
	}

	public createForm(model: any) {
		return this.formBuilder.group(model.toFormGroup());
	}

	public createFormGroup(form: any) {
		return this.formBuilder.group(form);
	}

	public checkResultError(result) {
		if (result) {
			return result.errors ? true : false;
		}
		return false;
	}
}
