import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(
    @Inject(SESSION_STORAGE) private storage:StorageService,
  ) {
  }

  public setStoreUserLogged(user: any): void {
    this.storage.set('currentUser', user);
  }

  public setAbrigoLogged(abrigo: any) {
    this.storage.set('currentAbrigo', abrigo);
  }

  public setVisitanteLogged(visitante: any) {
    this.storage.set('currentVisitante', visitante);
  }

  public getAbrigoLogged() {
    return this.storage.get('currentAbrigo');
  }

  public getStoreUserLogged() {
    return this.storage.get('currentUser');
  }

  public getVisitanteLogged() {
    return this.storage.get('currentVisitante');
  }

  public clearStorageUserLogged() {
    this.storage.remove('currentAbrigo');
    this.storage.remove('currentUser');
    this.storage.remove('currentVisitante');
    this.storage.remove('donations');
  }
}
