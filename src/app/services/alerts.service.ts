import swal from 'sweetalert2'
import { Injectable } from '@angular/core';

@Injectable()
export class AlertsService {

  constructor() {
  }

  confirmAlert(cb, title = 'Você tem certeza?', subtitle = 'Tem certeza que deseja realizar esta operação?') {
    swal({
			title: title,
			text: subtitle,
			type: 'question',
			showCancelButton: true,
			confirmButtonText: 'Sim!'
		}).then((result) => {
			if (result.value) {
				cb();
			}
		})
  }
}
