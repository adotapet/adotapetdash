import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonFunctionsService {

  constructor(
    private http: HttpClient
  ) { }

  public getEstados(): Observable<any> {
    return this.http.get('assets/estados-cidade.json');
  }

  public getPaises(): Observable<any> {
    return this.http.get('assets/paises.json');
  }

  public getRacasCachorro(): Observable<any> {
    return this.http.get('assets/racas_cachorros.json');
  }

  public getRacasGato(): Observable<any> {
    return this.http.get('assets/racas_gatos.json');
  }
}
