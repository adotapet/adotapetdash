import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, ResponseContentType } from '@angular/http';
import { DomSanitizer } from '../../../node_modules/@angular/platform-browser';

@Injectable()
export class AuthHttp {

  private serviceUrl = 'https://api-adota-pet.herokuapp.com/api';
  // private serviceUrl = 'https://v2-adota-pet.herokuapp.com/api'; // API v 2
  // private serviceUrl = 'http://192.168.25.230:1337/api';
  private userToken = '';
  public loggedUser: any;

  constructor(private http: Http, private sanitizer: DomSanitizer) {

	}

	public checkResultError(result) {
		if (result) {
			return result.errors ? true : false;
		}
		return false;
	}

  public post(url: string, data: any) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Content-Type', 'application/json');
			// header.append('Authorization', 'Bearer ' + String(this.userToken));
			const options = new RequestOptions({ headers: header });
			this.http.post(this.serviceUrl + url, data, options).subscribe((result: any) => {
				if (this.checkResultError(result)) {
          reject(result.json().message);
        } else {
					resolve(result.json());
				}
			},
			err => {
				reject(err);
			});
		});
	}

	public noAuthPost(url: string, data: any) {
		return new Promise((resolve, reject) => {
			this.http.post(this.serviceUrl + url, data).subscribe((result: any) => resolve(result.status),
			err => reject(err));
		});
	}

	public put(url: string, data: any) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Content-Type', 'application/json');
      // header.append('Authorization', 'Bearer ' + String(this.userToken));
			const options = new RequestOptions({ headers: header });
			this.http.put(this.serviceUrl + url, data, options).subscribe((result: any) => {
				if (this.checkResultError(result)) {
          reject(result.json().message);
        } else {
					resolve(result.json());
				}
			},
			err => {
				reject(err);
			});
		});
	}

  public getToken() {
    return this.userToken;
  }

	public delete(url: string) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
      header.append('Content-Type', 'application/json');
			// header.append('Authorization', 'Bearer ' + String(this.userToken));
			const options = new RequestOptions({ headers: header });

			this.http.delete(this.serviceUrl + url, options).subscribe((data: any) => {
				resolve(data);
			},
			err => {
				reject(err);
			});
		});
	}

	public get(url: string) {
		return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
      header.append('Content-Type', 'application/json');
			// header.append('Authorization', 'Bearer ' + String(this.userToken));
			const options = new RequestOptions({ headers: header });

			this.http.get(this.serviceUrl + url, options).subscribe((data: any) => {
				resolve(data.json());
			},
			(err) => {
				reject(err);
			});
		}).catch((err) => {
			console.log('ERROR', err);
		});
	}

	public auth(email: string, password: string) {
		const data  = { email: email, senha: password };
		return new Promise((resolve, reject) => {
			this.http.post(this.serviceUrl + 'login/', data).subscribe((result: any) => {
				const json: any = result.json();
				this.loggedUser = result.usuario;
				if (json.token) {
					this.userToken = json.token;
					resolve(result.json());
				}
			}, (err) => {
					console.log('Error on Auth');
					reject(err);
				}
			);
		});
	}

  public uploadFile(data) {
    return new Promise((resolve, reject) => {
			const header: Headers = new Headers();
			header.append('Content-Type', 'application/octet-stream');
			header.append('filename', String(new Date().getTime() + "_image.jpg"));
			// header.append('Authorization', 'Bearer ' + String(this.userToken));
			const options = new RequestOptions({ headers: header });
			this.http.post(this.serviceUrl + '/file', data , options).subscribe((result: any) => {
        console.log(result.json());
				resolve(result.json());
			},
			err => {
        console.log(err);
				reject(err);
			});
		});
  }

  public getFile(id) {
    return new Promise((resolve, reject) => {
			const options = new RequestOptions({ responseType: ResponseContentType.Blob });
			this.http.get(this.serviceUrl + '/file/' + id, options).subscribe((data: any) => {
				let fileBlob = data.blob();
				let safe = this.sanitizer.bypassSecurityTrustUrl(fileBlob);
        // let fileBlob = new Blob([data], {type: 'application/octet-stream'});
				resolve(fileBlob);
			},(err) => {
				reject(err);
			});
		}).catch((err) => {
			console.log('ERROR', err);
		});
  }

}
