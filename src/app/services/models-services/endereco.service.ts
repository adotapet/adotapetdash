import { Injectable } from '@angular/core';
import { AuthHttp } from '../../services/auth.http.service';
import { Endereco } from '../../models';


@Injectable()
export class EnderecoService {
	baseUrl = '/endereco';
  constructor(private authHttp: AuthHttp) {
  }

  public create(endereco: Endereco) {
    return this.authHttp.post(this.baseUrl, endereco);
  }

  public edit(endereco: Endereco) {
    return this.authHttp.put(`${this.baseUrl}/${endereco._id}`, endereco);
  }

  public read(id) {
    return this.authHttp.get(this.baseUrl);
	}

	public delete(endereco: Endereco) {
    return this.authHttp.delete(`/endereco/${endereco._id}`);
  }

  public getAll() {
    return this.authHttp.get(this.baseUrl);
  }

  public getOneById(id: string) {
    return this.authHttp.get(`${this.baseUrl}/${id}`);
  }

}
