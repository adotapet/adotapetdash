import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormUserFeedbackDirective } from './form-user-feedback.directive';
import { FormUserFeedbackComponent } from './form-user-feedback-component/form-user-feedback.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [ FormUserFeedbackDirective, FormUserFeedbackComponent ],
	entryComponents: [FormUserFeedbackComponent],
	exports: [ FormUserFeedbackDirective, FormUserFeedbackComponent ]
})
export class FormUserFeedbackModule { }
