import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-form-user-feedback',
	templateUrl: './form-user-feedback.component.html',
	styleUrls: ['./form-user-feedback.component.scss']
})
export class FormUserFeedbackComponent implements OnInit {

	errorMessage: string;

	constructor() { }

	ngOnInit() {
	}

	public setErrorMessage(field: string, error: string) {
		const textos = {
			invalidPassword: 'Senha deve ter 8 dígitos, uma letra maiúscula, minúscula, número e caracter especial!',
			required: `Campo '${field}' é obrigatório!`,
			minlength: `Campo '${field}' não atingiu o limite mínimo de caracteres!`,
			maxlength: `Campo '${field}' ultrapassou o limite de caracteres permitido!`,
			pattern: `Campo '${field}' não coincide com a regra!`,
			cpfValid: `Campo cpf inválido!`,
			cnpjValid: `Campo cnpj inválido!`,
			qtdResponsaveisValid: `Deve ter ao menos um responsável cadastrado!`,
			unique: `Campo '${field}' já cadastrado`,
			email: `E-mail inválido`,
		};
		this.errorMessage = textos[error];
	}

}
