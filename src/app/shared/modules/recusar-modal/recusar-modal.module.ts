import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecusarModalComponent } from './recusar-modal.component';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
  ],
  declarations: [RecusarModalComponent],
  exports: [RecusarModalComponent]
})
export class RecusarModalModule { }
