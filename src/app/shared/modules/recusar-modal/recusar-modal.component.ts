import { Component, OnInit, TemplateRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-recusar-modal',
  templateUrl: './recusar-modal.component.html',
  styleUrls: ['./recusar-modal.component.scss']
})
export class RecusarModalComponent implements OnInit {
  @ViewChild('modal') modal: any;
  @Output() recusado: EventEmitter<any> = new EventEmitter<any>();

  motivo: string = '';
  modalRef: any;
  constructor(private modalService: BsModalService) {}

  ngOnInit() {
  }

 
  open() {
    this.motivo = '';
    this.modalRef = this.modalService.show(this.modal);
  }

  recusar() {
    this.modalRef.hide(this.modalRef);
    this.recusado.emit(this.motivo);
  }

}
