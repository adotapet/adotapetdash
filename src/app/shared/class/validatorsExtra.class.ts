import { Validators, AbstractControl } from "../../../../node_modules/@angular/forms";
import { Injector } from "../../../../node_modules/@angular/core";

export class ValidatorsExtra extends Validators {

	private initialValue: any;

	isUnique(serviceClass: any, atributo, isEdit): any {
		return (control: AbstractControl) => {
			this.initialValue = this.initialValue ? this.initialValue : control.value;
			return serviceClass.getAll().then((result: any[]) => {
				let elFinded = result.find(el => control.value === el[atributo]);
				if (elFinded) {
					if (isEdit) {
						return elFinded[atributo] === this.initialValue ? null : {unique: false};
					} else {
						return {unique: false};
					}
				} else {
					return null;
				}
			});
		}
	}

	static isDate(control: AbstractControl) {
		let value = control.value;
		if (value) {
			let dia  = value.split("/")[0];
			let mes  = value.split("/")[1];
			let ano  = value.split("/")[2];
			if (isNaN(new Date(`${mes}-${dia}-${ano} 00:00:00`).getTime())) {
				return {invalidDate: 'Data Invalida'};
			}
		}
		return null;
	}

	static validarSenha(control: AbstractControl) {
		let value = control.value;
		let error = {invalidPassword: 'Senha deve ter umas parada loca'};
		if (value) {
			if(!value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)) {
				return error;
			}
		}
		return null;
	}

	static convertStringToDate(dateString: string) {
		let dia  = dateString.split("/")[0];
		let mes  = dateString.split("/")[1];
		let ano  = dateString.split("/")[2];
		return new Date(`${mes}-${dia}-${ano} 00:00:00`);
	}
}