export class MessagesCore {
    static tituloSuccess = 'Sucesso!';
    static tituloError = 'Erro!';
    static tituloWrning = 'Aviso!';

    static msgSuccesAdd = 'Dado inserido com sucesso!';
    static msgSuccesEdit = 'Dado editado com sucesso!';
    static msgSuccesDelete = 'Dado deletado com sucesso!';

    static msgErrorAdd = 'Erro ao inserir este dado!';
    static msgErrorEdit = 'Erro ao editar este dado!';
    static msgErrorDelete = 'Erro ao deletar este dado!';
}