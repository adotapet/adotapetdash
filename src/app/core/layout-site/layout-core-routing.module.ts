import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from '@angular/router';
import { LayoutSiteComponent } from './layout-site.component';
import { LayoutDashComponent } from '../layout-dash/layout-dash.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutSiteComponent,
    children: [
      { path: '', redirectTo: 'inicio' },
      { path: 'inicio', loadChildren: '../../modules-site/inicio/inicio.module#InicioModule' },
      { path: 'sobre-nos', loadChildren: '../../modules-site/sobre-nos/sobre-nos.module#SobreNosModule' },
      { path: 'como-funciona', loadChildren: '../../modules-site/como-funciona/como-funciona.module#ComoFuncionaModule' },
      { path: 'search', loadChildren: '../../modules-site/search/search.module#SearchModule'},
      { path: 'visitante', loadChildren: '../../modules-site/visitante/visitante.module#VisitanteModule'}
    ]
  },
  {
    path: 'dash',
    component: LayoutDashComponent,
    children: [
      { path: '', redirectTo: 'pet' },
      { path: 'inicio', loadChildren: '../../modules/inicio/inicio.module#InicioModule' },
      { path: 'abrigo', loadChildren: '../../modules/abrigo/abrigo-routing.module#AbrigoRoutingModule' },
      { path: 'pet', loadChildren: '../../modules/pet/pet.module#PetModule' },
      { path: 'adocao', loadChildren: '../../modules/adocoes/adocao.module#AdocaoModule' },
      { path: 'funcionario', loadChildren: '../../modules/funcionarios/funcionario.module#FuncionarioModule' },
      { path: 'doacao', loadChildren: '../../modules/doacoes/doacao.module#DoacaoModule' },
      { path: 'solicitacoes', loadChildren: '../../modules/solicitacoes/solicitacao.module#SolicitacaoModule' },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class LayoutCoreRoutingModule { }
