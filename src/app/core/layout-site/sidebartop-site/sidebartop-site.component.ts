import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { Usuario } from '../../../models/usuario.model';

import { AbrigoService } from '../../../modules/abrigo/abrigo.service';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { UsuarioService } from '../../../modules/usuario/usuario.service';

import { Abrigo, Endereco } from '../../../models';
import { Validators, FormGroup } from '@angular/forms';
import { FormCustomValidator } from '../../../services/form-custom-validator.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from '../../../services/local-storage.service';
import { MaskService } from '../../mask.service';
import { EnderecoService } from '../../../services/models-services/endereco.service';
import { MessagesCore } from '../../../shared/class/messagesCore.class';
import { Funcionario } from '../../../models/funcionario.model';
import { FuncionarioService } from '../../../modules/funcionarios/funcionario.service';
import { Visitante } from 'src/app/models/visitante.model';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-sidebartop-site',
  templateUrl: './sidebartop-site.component.html',
  styleUrls: ['./sidebartop-site.component.scss']
})
export class SidebartopSiteComponent implements OnInit {

  abrigo: Abrigo = new Abrigo();
  novoUsuario: Usuario = new Usuario();
  endereco: Endereco = new Endereco();
  funcionario: Funcionario = new Funcionario();

  visitante: Visitante = new Visitante();
  usuarioVisitante: Usuario = new Usuario();
  enderecoVisitante: Endereco = new Endereco();

  modalCadastro: BsModalRef;
  nomeInput = '';
  emailInput = '';
  usuarioInput = '';
  senhaInput = '';
  confSenhaInput = '';

  modalLogin: BsModalRef;
  usernameInput = '';
  passwordInput = '';

  etapasCadastro: any;
  etapa: any;

  cidades = [];
  estados = [];
  paises = [];
  jsonEstadoData: any;

  formLogin: FormGroup;
  formNewUsuario: FormGroup;
  formNewAbrigo: FormGroup;
  formNewEndereco: FormGroup;
  formNewFuncionario: FormGroup;

  formNewVisitante: FormGroup;
  formNewUsuarioVisitante: FormGroup;
  formNewEnderecoVisitante: FormGroup;

  mask:any = {};

  blobPerfil: any;
  constructor(
    private modalService: BsModalService,
    private router: Router,
    private authHttp: AuthHttp,
    private common: CommonFunctionsService,
    private abrigoService: AbrigoService,
    private usuarioService: UsuarioService,
    private enderecoService: EnderecoService,
    private funcionarioService: FuncionarioService,
    private formValidator: FormCustomValidator,
    private toast: ToastrService,
    private storage: LocalStorageService,
    private maskService: MaskService,
    private visitanteService: VisitanteService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    const services = [];
    services['usuario'] = this.usuarioService;
    services['abrigo'] = this.abrigoService;
    this.formLogin = this.formValidator.createFormGroup(this.toFormGroupLogin());
    this.formNewUsuario = this.formValidator.createFormGroup(this.novoUsuario.toFormGroup(services));
    this.formNewAbrigo = this.formValidator.createFormGroup(this.abrigo.toFormGroup(services));
    this.formNewEndereco = this.formValidator.createFormGroup(this.endereco.toFormGroup());
    this.formNewFuncionario = this.formValidator.createFormGroup(this.funcionario.toFormGroup());

    this.formNewVisitante = this.formValidator.createFormGroup(this.visitante.toFormGroup());
    this.formNewEnderecoVisitante = this.formValidator.createFormGroup(this.enderecoVisitante.toFormGroup());
    this.formNewUsuarioVisitante = this.formValidator.createFormGroup(this.usuarioVisitante.toFormGroup(services));

    this.loadEtapasCadastro();
    this.initMask();
    this.etapa = this.etapasCadastro.PRIMEIRA;

    this.common.getPaises().subscribe((result) => {
      this.paises = result.filter((elem) => elem.nome_pais === 'Brasil');
    });

    this.common.getEstados().subscribe((data) => {
      this.jsonEstadoData = data.estados;
      this.estados = data.estados;
    });
    this.getLoggedVisitante();
  }

  getLoggedVisitante() {
    this.visitanteService.visitanteLogged = this.storage.getVisitanteLogged();
    if (!this.visitanteService.visitanteLogged || !this.storage.getVisitanteLogged()) {
      this.router.navigate(['/inicio']);
      return;
    } else {
      console.log('Visitante logado', this.visitanteService.visitanteLogged);
      this.carregarFoto();
    }
  }

  carregarFoto() {
    if (this.visitanteService.visitanteLogged) {
      this.authHttp.getFile(this.visitanteService.visitanteLogged.fotoPerfil ? this.visitanteService.visitanteLogged.fotoPerfil._id || this.visitanteService.visitanteLogged.fotoPerfil : null).then(result => {
        this.blobPerfil = result;
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) :
    '../../../../assets/dog-icon.png';
  }

  initMask() {
    this.mask.email = this.maskService.emailMask;
    this.mask.cnpj = this.maskService.cnpjMask;
    this.mask.telefone = this.maskService.telefoneMask;
    this.mask.cep = this.maskService.cepMask;
    this.mask.data = this.maskService.dateMask;
  }

  loadEtapasCadastro() {
    this.etapasCadastro = {
      PRIMEIRA: 1,
      ABRIGO: 2,
      ADOTANTE: 3,
    };
  }

  openRegisterModal(modal: TemplateRef<any>) {
    this.modalCadastro = this.modalService.show(modal, {class: 'modal-lg'});
    // this.modalCadastro = this.modalService.show(modal);
  }

  openLogInModal(modal: TemplateRef<any>) {
    this.modalLogin = this.modalService.show(modal);
  }

  register() {
    if (this.formNewUsuario.valid && this.formNewAbrigo.valid && this.formNewEndereco.valid) {
      if (this.formNewUsuario.get('password').value !== this.formNewUsuario.get('confpassword').value) {
        this.toast.warning('As senhas digitadas não conferem!');
        return;
      }
      let backups: any = {}; // armazena os IDS, caso dê algum problema, deleta do banco
      this.novoUsuario.fromFormGroup(this.formNewUsuario);
      this.abrigo.fromFormGroup(this.formNewAbrigo);
      this.endereco.fromFormGroup(this.formNewEndereco);
      this.funcionario.fromFormGroup(this.formNewFuncionario);
      // salva o usuario
      const loading = this.toast.info('Carregando');
      this.usuarioService.create(this.novoUsuario)
      .then((resultUsuario: any) => {
        console.log('resultUsuario', resultUsuario);
        if (this.formValidator.checkResultError(resultUsuario)) {
          throw new Error(resultUsuario.message);
        }
        backups['usuario'] = resultUsuario;
        // let funcionario = new Funcionario();
        this.funcionario.usuario = resultUsuario._id;
        return this.funcionarioService.create(this.funcionario);
      })
      .then((resultFuncionario: any) => {
        console.log('resultFuncionario', resultFuncionario);
        if (this.formValidator.checkResultError(resultFuncionario)) {
          throw new Error(resultFuncionario.message);
        }
        backups['funcionario'] = resultFuncionario;
        return this.enderecoService.create(this.endereco);
      })
      .then((resultEndereco: any) => {
        console.log('resultUsuario', resultEndereco);
        if (this.formValidator.checkResultError(resultEndereco)) {
          throw new Error(resultEndereco.message);
        }
        backups['endereco'] = resultEndereco;
        this.abrigo.endereco = backups['endereco']._id;
        this.abrigo.funcionarios.push(backups['funcionario']._id);
        return this.abrigoService.create(this.abrigo);
      })
      .then((resultAbrigo: any) => {
        console.log('resultAbrigo', resultAbrigo);
        if (this.formValidator.checkResultError(resultAbrigo)) {
          throw new Error(resultAbrigo.message);
        }
        backups['abrigo'] = resultAbrigo;
        this.usuarioService.setUser(backups['usuario']);
        this.resetForms();
        this.modalCadastro.hide();
        this.toast.clear(loading.toastId);
        this.toast.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesAdd);
      })
      .catch(err => {
        this.toast.clear(loading.toastId);
        console.log(`catch`, err);
        this.deleteErros(backups);
        this.toast.error(MessagesCore.tituloError, err);
        // this.resetForms();
      });
      // this.usuarioService.create(this.novoUsuario).then((result: any) => {
      //   this.registrarAbrigo(result._id);
      //   this.usuarioService.setUser(result);
      // }).catch((err) => {
      //   console.log(err);
      //   alert('Dados inválidos');
      //   this.resetForms();
      // });
    } else {
      this.toast.error('Formulário inválido! Corrija os campos incorretos.');
      console.log(this.formNewAbrigo, this.formNewEndereco, this.formNewUsuario);
    }
  }

  registerVisitante() {
    if (this.formNewUsuarioVisitante.valid && this.formNewVisitante.valid && this.formNewEnderecoVisitante.valid) {
      if (this.formNewUsuarioVisitante.get('password').value !== this.formNewUsuarioVisitante.get('confpassword').value) {
        this.toast.warning('As senhas digitadas não conferem!');
        return;
      }
      let backups: any = {}; // armazena os IDS, caso dê algum problema, deleta do banco
      this.visitante.fromFormGroup(this.formNewVisitante);
      this.enderecoVisitante.fromFormGroup(this.formNewEnderecoVisitante);
      this.usuarioVisitante.fromFormGroup(this.formNewUsuarioVisitante);
      // salva o usuario
      console.log(this.visitante, this.enderecoVisitante, this.usuarioVisitante);
      const loading = this.toast.info('Carregando');
      this.usuarioService.create(this.usuarioVisitante)
      .then((resultUsuario: any) => {
        backups['usuario'] = resultUsuario;
        this.visitante.usuario = resultUsuario._id;
        return this.enderecoService.create(this.enderecoVisitante);
      })
      .then((resultEndereco: any) => {
        backups['endereco'] = resultEndereco;
        this.visitante.endereco = resultEndereco._id;
        return this.visitanteService.create(this.visitante);
      })
      .then((resultVisitante: any) => {
        backups['visitante'] = resultVisitante;
        this.resetForms();
        this.modalCadastro.hide();
        this.toast.clear(loading.toastId);
        this.toast.success(MessagesCore.tituloSuccess, MessagesCore.msgSuccesAdd);
      })
      .catch(err => {
        this.toast.clear(loading.toastId);
        console.log(`catch`, err);
        this.deleteErros(backups);
        this.toast.error(MessagesCore.tituloError, err);
      });
    } else {
      this.toast.error('Formulário inválido! Corrija os campos incorretos.');
      this.formValidator.validateAllFormFields(this.formNewEnderecoVisitante);
      this.formValidator.validateAllFormFields(this.formNewVisitante);
      this.formValidator.validateAllFormFields(this.formNewUsuarioVisitante);
    }
  }

  deleteErros(backups: any[]) {
    if (backups['usuario']) {
      this.usuarioService.delete(backups['usuario']).then(() => delete backups['usuario']);
    }
    if (backups['abrigo']) {
      this.abrigoService.delete(backups['abrigo']).then(() => delete backups['abrigo']);
    }
    if (backups['endereco']) {
      this.enderecoService.delete(backups['endereco']).then(() => delete backups['endereco']);
    }
    if (backups['funcionario']) {
      this.funcionarioService.delete(backups['funcionario']).then(() => delete backups['funcionario']);
    }
    if (backups['visitante']) {
      this.visitanteService.delete(backups['visitante']).then(() => delete backups['visitante']);
    }
  }

  registrarAbrigo(usuario_id) {
    let self = this;
    this.abrigo.funcionarios.push(usuario_id);
    this.abrigo.fromFormGroup(this.formNewAbrigo);
    this.abrigoService.create(this.abrigo).then((result) => {
      this.resetForms();
      this.modalCadastro.hide();
      setTimeout(() => {
        this.toast.success('Cadastro Realizado com sucesso');
        // self.router.navigate(['/dash']).catch(err => console.log(err));
      }, 1000);
    }).catch((err) => {
      console.log(err);
      this.resetForms();
      alert('Dados inválidos');
    });
  }

  validate() {
    return (this.emailInput !== '' && this.usuarioInput !== '' && this.senhaInput !== '');
    // return true;
  }

  async logIn() {
    const self = this;
    if (this.formLogin.valid){
      const loading = this.toast.info('Carregando');
      this.usuarioService.getAll().then((res: any) => {
        const username = this.formLogin.value.usuario;
        const password = this.formLogin.value.senha;
        console.log(res, username, password);
        const user = res.find((elem) => ((elem.username === username) || (elem.email == username)) && elem.password === password);
        console.log('user', user);
        if (user != null) {
          this.usuarioService.setUser(user);
          this.abrigoService.getAbrigoByUserID(user._id).then(result => {
            if (result) {
              this.usuarioService.setAbrigo(result);
              this.storage.setAbrigoLogged(result);
              this.storage.setStoreUserLogged(user);
              this.modalLogin.hide();
              this.toast.clear(loading.toastId);
              setTimeout(() => {
                self.router.navigate(['/dash']).catch(err => console.log(err));
              }, 500);
            } else {
              this.visitanteService.getVisitanteByUserID(user._id).then(resultVisitante => {
                console.log('visitante', resultVisitante);
                if (resultVisitante) {
                  this.visitanteService.visitanteLogged = resultVisitante;
                  this.storage.setVisitanteLogged(resultVisitante);
                  this.storage.setStoreUserLogged(user);
                  this.carregarFoto();
                  this.modalLogin.hide();
                  this.toast.clear(loading.toastId);
                  this.formLogin.reset();
                }
              });
            }
          });
        } else {
          this.toast.clear(loading.toastId);
          this.toast.warning('Nenhum usuário encontrado!');
        }
      }).catch((err) => {
        console.log(err);
        this.toast.clear(loading.toastId);
        this.toast.error('Erro ao realizar login!');
      });
    } else {
      this.toast.error('Formulário inválido!');
    }
    return false;
  }

  changeEtapa(etapa) {
    this.etapa = etapa;
  }

  updateCidades(estado) {
    this.cidades = estado.cidades;
  }

  voltarEtapa() {
    this.etapa = this.etapasCadastro.PRIMEIRA;
  }

  toFormGroupLogin() {
    return {
      usuario:  [null, Validators.required],
      senha:    [null, Validators.required],
    }
  }

  fromFormGroupLogin(obj, form: FormGroup) {
    obj.usuario = form.value.usuario;
    obj.senha = form.value.senha;
  }

  teclaPess(tecla) {
    if (tecla.keyCode === 13) {
      this.register();
    }
  }

  resetForms() {
    this.formLogin.reset();
    this.formNewUsuario.reset();
    this.formNewAbrigo.reset();
    this.formNewEndereco.reset();
    this.formNewFuncionario.reset();
    this.formNewVisitante.reset();
    this.formNewUsuarioVisitante.reset();
    this.formNewEnderecoVisitante.reset();
  }

  sobreNos() {
    this.router.navigate(['/inicio'], { fragment: 'about' });
  }

  depoimentos() {
    this.router.navigate(['/inicio'], { fragment: 'depoimentos' });
  }

  comoFunciona() {
    this.router.navigate(['/inicio'], { fragment: 'comofunciona' });
  }

  editarVisitante() {
    this.router.navigate(['/visitante']);
  }

  underConstruction() {
    alert('Esta página ainda está em construção');
  }

  logout() {
    this.usuarioService.setUser(null);
    this.blobPerfil = null;
    this.visitanteService.visitanteLogged = null;
    this.storage.clearStorageUserLogged();
    this.router.navigate(['/inicio']);
  }
}
