import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutSiteComponent } from './layout-site.component';
import { LayoutCoreRoutingModule } from './layout-core-routing.module';
import { SidebartopSiteComponent } from './sidebartop-site/sidebartop-site.component';
import { LayoutDashComponent } from '../layout-dash/layout-dash.component';
import { SidebartopDashComponent } from '../layout-dash/sidebartop-dash/sidebartop-dash.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonFunctionsService } from '../../services/common-functions.service';

import { UsuarioModule } from '../../modules/usuario/usuario.module';
import { AbrigoModule } from '../../modules/abrigo/abrigo.module';
import { AuthHttp } from '../../services/auth.http.service';
import { TabsModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormCustomValidator } from '../../services/form-custom-validator.service';
import { FormUserFeedbackModule } from '../../shared/modules/form-user-feedback/form-user-feedback.module';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from '../../services/local-storage.service';
import { MaskService } from '../mask.service';
import { TextMaskModule } from 'angular2-text-mask';
import { PetModule } from '../../modules/pet/pet.module';
import { EnderecoService } from '../../services/models-services/endereco.service';
import { FuncionarioService } from '../../modules/funcionarios/funcionario.service';
import { VisitanteService } from 'src/app/modules/visitante/visitante.service';

@NgModule({
  imports: [
    TextMaskModule,
    CommonModule,
    LayoutCoreRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgSelectModule,
    UsuarioModule.forRoot(),
    PetModule.forRoot(),
    AbrigoModule.forRoot(),
    TabsModule.forRoot(),
    FormUserFeedbackModule
  ],
  declarations: [LayoutSiteComponent, LayoutDashComponent, SidebartopSiteComponent, SidebartopDashComponent, SidebarComponent],
  providers: [
    AuthHttp,
    CommonFunctionsService,
    FormCustomValidator,
    ToastrService,
    LocalStorageService,
    MaskService,
    EnderecoService,
    FuncionarioService,
    VisitanteService
  ]
})
export class LayoutCoreModule { }
