import { Injectable } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import emailMask from 'text-mask-addons/dist/emailMask';

@Injectable()
export class MaskService {
	public cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
	public cepMask = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
	public cnpjMask = [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];
	public placaMask = [/[A-Za-z]/, /[A-Za-z]/, /[A-Za-z]/, '-', /\d/, /\d/, /\d/, /\d/];
	public hexColorMask = ['#', /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/, /[A-Za-z0-9]/];
	public dateMask = [/[0-3]/, /[0-9]/, '/', /[0-1]/, /[0-9]/, '/', /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
	public emailMask = emailMask;
	public moneyMask = createNumberMask({
		thousandsSeparatorSymbol: '.',
		allowDecimal: true,
		decimalSymbol: ',',
		prefix: 'R$' 
	})
	constructor() {
	}

	public telefoneMask (text) {
		if (text.charAt(text.length - 1) !== '_') {
			return ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
		} else {
			return ['(', /\d/, /\d/, ')',' ',/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
		}
	}

	public dateMaskDatePicker(event) {
		let value = event.target.value;
		value = value.replace(/\D/g, '');
		value = value.replace(/(\d{2})(\d)/, '\$1/\$2');
		value = value.replace(/(\d{2})\/(\d{2})(\d)/, '\$1/\$2/\$3');
		value = value.replace(/(\d{2})\/(\d{2})\/(\d{4})(\d)/, '\$1/\$2/\$3');
		event.target.value = value;
	}
}

