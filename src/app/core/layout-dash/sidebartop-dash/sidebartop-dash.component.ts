import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsuarioService } from '../../../modules/usuario/usuario.service';
import { AbrigoService } from '../../../modules/abrigo/abrigo.service';
import { LocalStorageService } from '../../../services/local-storage.service';
import { AuthHttp } from 'src/app/services/auth.http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { FuncionarioService } from 'src/app/modules/funcionarios/funcionario.service';

@Component({
  selector: 'app-sidebartop-dash',
  templateUrl: './sidebartop-dash.component.html',
  styleUrls: ['./sidebartop-dash.component.scss']
})
export class SidebartopDashComponent implements OnInit {
  private usuario: any;
  private abrigo: any;
  private blobPerfil: any;
  private funcionario: any;
  constructor(
    private router: Router,
    public userService: UsuarioService,
    public abrigoService: AbrigoService,
    public storage: LocalStorageService,
    private authHttp: AuthHttp,
    private sanitizer: DomSanitizer,
    private funcionarioService: FuncionarioService
  ) {
    // this.router.navigate(['/dash/abrigo/read']);

   }

  async ngOnInit() {
    this.userService.setUser(this.storage.getStoreUserLogged());
    this.usuario = this.userService.getUser();
    if (!this.usuario || !this.storage.getStoreUserLogged()) {
      this.router.navigate(['/inicio']);
      return;
    }
    if (!this.storage.getAbrigoLogged()) {
      this.router.navigate(['/inicio']);
      return;
    } else {
      let abrigo = this.storage.getAbrigoLogged();
      console.log('Abrigo enconrado', abrigo);
      this.abrigoService.abrigo = abrigo;
      this.abrigo = abrigo;
      this.carregarFoto();
    }
    
    this.funcionario = await this.funcionarioService.getFuncionarioByUserID(this.usuario._id);
  }

  goToAbrigoRead() {
    this.router.navigate(['/dash/abrigo/read']);
  }

  logout() {
    this.userService.setUser(null);
    this.storage.clearStorageUserLogged();
    this.router.navigate(['/inicio']);
  }

  carregarFoto() {
    if (this.abrigo.fotoPerfil) {
      this.authHttp.getFile(this.abrigo.fotoPerfil ? this.abrigo.fotoPerfil._id || this.abrigo.fotoPerfil : null).then(result => {
        this.blobPerfil = result;
      }).catch(err => {
        console.log(err);
      });
    }
  }

  getFoto() {
    return this.blobPerfil ? this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(this.blobPerfil)) : 
    '../../../../assets/dog-icon.png';
  }
  
}
