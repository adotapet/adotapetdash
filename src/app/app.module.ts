import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { ModalModule, TabsModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { InicioComponent } from './modules/inicio/inicio.component';
import { FormsModule } from '@angular/forms';

import { AuthHttp } from './services/auth.http.service';
import { UsuarioModule } from './modules/usuario/usuario.module';
import { ToastrModule } from 'ngx-toastr';
import { StorageServiceModule } from 'angular-webstorage-service';
import { RecusarModalComponent } from './shared/modules/recusar-modal/recusar-modal.component';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    StorageServiceModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    UsuarioModule,
    HttpClientModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule, // required animations module
  ],
  providers: [
    AuthHttp,
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
